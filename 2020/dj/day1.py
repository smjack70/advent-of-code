# Open file, read numbers, store in array
# read first number
# if first number plus next in array == 2020 win
# else skip

def part1(numbers):
  for i in numbers:
    sum = i
    for j in numbers:
      if sum == j:
        break
      else:
        sum = sum + j
        if sum == 2020:
          print("win ", j, " and ", i)
          product = i * j
          print("Product: ", product)
        else:
          sum = i;

def part2(numbers):
  for i in numbers:
    sum = i
    for j in numbers:
      if sum == j:
        break
      else:
        for n in numbers:
          if sum == n:
            break
          else:
            sum = sum + j + n
            if sum == 2020:
              print("win ", i, " ", j, " ", n)
              product = i * j * n
              print("product: ", product)

            else:
              sum = i;


file = open('real_data', 'r')

data = file.readlines()
nums = []
for line in data:
  nums.append(int(line.strip()))
#part1(nums)
part2(nums)