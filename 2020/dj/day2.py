file = open('day2_real', 'r')
data = file.readlines()
nums = []

def passwd_check(input):
  minmax = input.split()[0]
  min = minmax.split('-')[0]
  max = minmax.split('-')[1]

  ltr = input.split()[1][0]
  passwd = input.split()[2]

  if passwd.count(ltr) >= int(min) and passwd.count(ltr) <= int(max):
    return True
  else:
    return False

def passwd_check2(input):
  minmax = input.split()[0]
  chk1 = minmax.split('-')[0]
  chk2 = minmax.split('-')[1]

  ltr = input.split()[1][0]
  passwd = input.split()[2]

  print(int(chk1), ': ', passwd[int(chk1)-1], " v " , int(chk2), ': ', passwd[int(chk2)-1])
  if int(chk2)-1 < len(passwd):
    if (passwd[int(chk1)-1] == ltr):
      if (passwd[int(chk2)-1] != ltr):
        return True
    elif (passwd[int(chk2)-1] == ltr):
      if (passwd[int(chk1)-1] != ltr):
        return True
    else:
      return False
  return False

result = 0
for line in data:
  if passwd_check2(line.strip()):
    result += 1


print(result)
  
#part1(nums)
#part2(nums)