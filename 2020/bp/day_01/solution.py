
from typing import List, Tuple, Iterator
import sys

def read_file(filename: str) -> List[int]:
    rayscanner = []
    for line in open(filename, 'r'):
        num = int(line.strip())
        rayscanner.append(num)
    return rayscanner

def find_sum_og(arr: List[int], sum: int) -> Tuple[int, int]:
    for i in range(len(arr)):
        for j in arr[i:]:
            if arr[i] + j == sum:
                return (arr[i], j)


def _copy_list(arr: List[int], num_copies: int) -> List[Iterator]:
    if num_copies <= 1:
        return arr
    
    new_arr = []
    for _ in range(num_copies):
        new_arr.append(iter(arr))
    
    return new_arr

def find_sum(arr:List[int], sum:int, num_copies: int) -> List[int]:
    indexes = [0] * num_copies
    done = False
    while not done:
        if is_computable(indexes):
            test_sum = 0
            for i in indexes:
                test_sum += arr[i]

            if test_sum == sum:
                dun = []
                for i in indexes:
                    dun.append(arr[i])
                return dun

        increment(indexes, len(arr))
        done = is_work_done(indexes, len(arr))

def is_computable(indexes:List[int]) -> bool:
    return len(set(indexes)) == len(indexes)

def is_work_done(indexes:List[int], len_list:int) -> bool:
    return (indexes[-1] == len_list - 1)

def increment(indexes:List[int], list_len: int) -> None:
    increment_next = True

    for i in range(len(indexes)):
        if increment_next:
            indexes[i] += 1
            increment_next = False

        if indexes[i] == list_len:
            indexes[i] = 0
            increment_next = True

if __name__ == '__main__':
    nums = read_file(sys.path[0] + '\\input')
    ints = find_sum(nums, 2020, 3)
    print(ints)
    p = 1
    for i in ints:
        p *= i
    print(p)
