

class Password:
    def __init__(self, mn:str, mx:str, ch:str, pw:str):
        self.min = int(mn)
        self.max = int(mx)
        self.ch = ch
        self.pw = pw

    def is_password_sled_valid(self) -> bool:
        count = self.pw.count(self.ch)
        return count >= self.min and count <= self.max
    
    def is_password_toboggan_valid(self) -> bool:
        pos1 = self.pw[self.min - 1] == self.ch
        pos2 = self.pw[self.max - 1] == self.ch
        return pos1 ^ pos2