from re import match
from typing import List
from password import Password
import sys

def read_file(filename: str) -> List[int]:
    sled = 0
    toboggan = 0
    for line in open(filename, 'r'):
        matches = match(r'(\d+)-(\d+)\s(\w):\s(\w+)', line)
        p = Password(matches[1], matches[2], matches[3], matches[4])
        if p.is_password_sled_valid():
            sled += 1
        if p.is_password_toboggan_valid():
            toboggan += 1
    return [sled, toboggan]

if __name__ == '__main__':
    nums = read_file(sys.path[0] + '\\input')
    print(nums)