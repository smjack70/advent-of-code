import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class day10a {

    public static void main(String[] args) {
        
        List<Integer> adapters = new ArrayList<Integer>();
        try
        {
            String filePath = AdventUtil.getPathTo("day10_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                adapters.add(Integer.parseInt(line));
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        Collections.sort(adapters);
        int oneJump = 0;
        int threeJump = 1; // the last adapter jump is always three
        int last = 0;
        for (Integer x : adapters)
        {
           int next = x.intValue();
           int diff = next - last;
           switch (diff)
           {
           case 1:
               oneJump++;
               break;
           case 3:
               threeJump++;
               break;
           default:
                   break;
           }
           last = next;
        }
        System.out.println(oneJump * threeJump);

                
    }

}
