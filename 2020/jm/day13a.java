import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day13a {
    
    static String OUT_OF_SERVICE = "x";


    public static void main(String[] args) {
        
        int startTime = 0;
        List<Integer> busIds = new ArrayList<Integer>();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day13_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            line = reader.readLine();
            startTime = Integer.parseInt(line);
            String[] schedule = reader.readLine().split(",");
            for (String busId : schedule)
            {
                if ( busId.equals(OUT_OF_SERVICE) )
                {
                    continue;
                }
                busIds.add(Integer.parseInt(busId));
            }

            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int leaveTime = Integer.MAX_VALUE;
        int thisBus = 0;
        for (int busId : busIds)
        {
            int preTime = (startTime / busId) * busId;
            
            if (preTime < startTime)
            {
                preTime += busId;
            }
            if (preTime < leaveTime)
            {
                leaveTime = preTime;
                thisBus = busId;
            }
        }
        System.out.println((leaveTime - startTime) * thisBus);
    }

}
