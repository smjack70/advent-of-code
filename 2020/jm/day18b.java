import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class day18b {
    
    private static final char MULT    = '*';
    private static final char PLUS    = '+';
    private static final char NO_OP   = '#';
    private static final char L_PAREN = '(';
    private static final char R_PAREN = ')';
    private static final char SPACE   = ' ';
    
    private static int getParenClose(String str, int start)
    {
        int parenCount = 0;
        int index = start + 1;
        while (index < str.length())
        {
            char current = str.charAt(index); 
            switch (current)
            {
            case L_PAREN:
                parenCount++;
                break;
            case R_PAREN:
                if (parenCount > 0)
                {
                    parenCount--;
                }
                else {
                    return index;
                }
                break;
            default:
                break;
            }
            
            index++;
        }
        return -1;
    }
    
    private static void calc(Stack<Long> stack, char operator)
    {
        if ( (operator == NO_OP) || (stack.size() < 2 ) )
        {
            return;
        }
        
        long v1 = stack.pop();
        long v2 = stack.pop();
        
        switch (operator)
        {
        case MULT:
            stack.push( v1 * v2 );
            break;
        case PLUS:
            stack.push(v1 + v2);
            break;
        default:
            System.out.println("How did we get here? " + v1 + operator + v2);
            break;
        }
        return;
    }
    
    
    private static long solveEquation(String equation)
    {
        Stack<Long> compStack = new Stack<Long>();
        char operator = NO_OP;
        StringBuilder builder = new StringBuilder();
        int index = 0;
        while (index < equation.length())
        {
            char current = equation.charAt(index);
            switch (current)
            {
            case L_PAREN:
                int rIndex = getParenClose(equation, index);
                long value = solveEquation(equation.substring(index + 1, rIndex));
                compStack.push(value);
                index = rIndex + 1;
                break;
            case R_PAREN:
                System.out.println("??? R_PAREN at index [" + index + "] in [" + equation + "]");
                index = index + 1;
                break;
            case MULT:
                /**
                 * This is where it gets wild. Since the defined order of 
                 * operations is parens -> addition -> multiplication, we presume 
                 * everything to our "left" has been evaluated, so we need to
                 * distinctly evalutate everything to our 'right' before
                 * doing a multiplication operation (which happens at the end).   
                 */
                compStack.push(solveEquation(equation.substring(index + 1)));
                index = equation.length();
                break;
            case PLUS:
                operator = PLUS;
                index = index + 1;
                continue;
            case SPACE:
                if (builder.length() > 0)
                {
                    long newValue = Long.parseLong(builder.toString());
                    builder.setLength(0);
                    compStack.push(newValue);
                }
                index = index + 1;
                break;
            default:
                builder.append(current);
                index = index + 1;
                break;
            }
            
            if ( (operator != NO_OP) && (compStack.size() >= 2) )
            {
                calc(compStack, operator);
                operator = NO_OP;
            }
        }
        
        if (builder.length() > 0)
        {
            long newValue = Long.parseLong(builder.toString());
            builder.setLength(0);
            compStack.push(newValue);
        }
        
        calc(compStack, operator);
        
        do 
        {
            calc(compStack, MULT);
        } while (compStack.size() > 1);

        long value = compStack.pop();
        return value;
    }

    public static void main(String[] args) {

        List<Long> results = new ArrayList<Long>();
        try
        {
            String filePath = AdventUtil.getPathTo("day18_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            while ((line = reader.readLine()) != null)
            {
                results.add(solveEquation(line));
            }
            
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        long finalValue = 0;
        for (long val : results)
        {
            finalValue += val;
            
        }
        System.out.println(finalValue);
        
        
    } // end main

}
