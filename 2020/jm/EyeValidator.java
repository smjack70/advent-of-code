import java.util.Arrays;

public class EyeValidator implements ValidateData {
	
	String[] hairColors = {"amb","blu","brn","gry", "grn","hzl","oth"};
	
	@Override
	public boolean validate(String data){
		return Arrays.asList(hairColors).contains(data);

	}

}
