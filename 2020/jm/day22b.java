import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class day22b {
    
    public enum Player
    {
        PLAYER1,
        PLAYER2,
    }
    
    private static class Deck
    {
        private static StringBuilder builder = new StringBuilder();
        private Deque<Integer> deck = new ArrayDeque<Integer>();
        
        
        public Deck()
        {
            
        }
        
        public Deck(Deck copy, int size)
        {
            Iterator<Integer> iter = copy.deck.iterator();
            for (int i = 0; i < size; i++)
            {
                deck.add(iter.next().intValue() );
            }
        }
        
        public int size()
        {
            return deck.size();
        }
        
        public void add(int value)
        {
            deck.add(value);
        }
        
        public int draw()
        {
            return deck.pop();
        }
        
        public String getFingerprint()
        {
            builder.setLength(0);
            for (int x : deck)
            {
                builder.append(x);
                builder.append(',');
            }
            return builder.toString();
        }
        
    }
    
    private static class Fingerprints
    {
        
        private Set<String> prints = new HashSet<String>();

        public boolean hasFingerprint(Deck deck1, Deck deck2)
        {
            String print = deck1.getFingerprint() + ":" + deck2.getFingerprint(); 
            if (prints.contains(print))
            {
                return true;
            }

            prints.add(print);
            return false;
        }

    }
    
    private static Player runGame(Deck deck1, Deck deck2)
    {
        Fingerprints fp = new Fingerprints();
        Player winner = Player.PLAYER1;
        while (deck1.size() > 0 && deck2.size() > 0)
        {
            if (true == fp.hasFingerprint(deck1,deck2))
            {
                return Player.PLAYER1;
            }
            int d1 = deck1.draw();
            int d2 = deck2.draw();
            
            if (d1 <= deck1.size() && d2 <= deck2.size())
            {
                Deck copy1 = new Deck(deck1, d1);
                Deck copy2 = new Deck(deck2, d2);
                winner = runGame(copy1, copy2);
            }
            else
            {
                if (d1 > d2)
                {
                    winner = Player.PLAYER1;
                }
                else
                {
                    winner = Player.PLAYER2;
                }
            }
            
            switch (winner)
            {
            case PLAYER1:
                deck1.add(d1);
                deck1.add(d2);
                break;
            case PLAYER2:
                deck2.add(d2);
                deck2.add(d1);
                break;
            }


        }
        return winner;
    }
    
    public static void main(String[] args) {

        Deck deck1 = new Deck();
        Deck deck2 = new Deck();
        try
        {
            String filePath = AdventUtil.getPathTo("day22_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            reader.readLine(); // Read off the player line
            while ((line=reader.readLine()).isEmpty() == false)
            {
                deck1.add(Integer.parseInt(line));
            }
            
            reader.readLine(); // Read off the player line
            while ((line = reader.readLine()) != null)
            {
                deck2.add(Integer.parseInt(line));
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        Player winner = runGame(deck1, deck2);
        Deck deck = null;
        
        switch (winner)
        {
        case PLAYER1:
            deck = deck1;
            break;
        case PLAYER2:
            deck = deck2;
            break;
        }
        
        int value = 0;
        for (int i = deck.size(); i > 0; i --)
        {
            value += (deck.draw() * i);
        }
        
        System.out.println(value);
        
        
    } // end main

}
