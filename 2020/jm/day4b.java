import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class day4b {
    
    public static void main(String[] args) throws Exception{
        
        List<Map<String, String>> passportList = new ArrayList<Map<String, String>>();
        
        Map<String, ValidateData> validatorMap = new HashMap<String, ValidateData>();
        validatorMap.put("byr", new BirthYearValidator());
        validatorMap.put("iyr", new IssueYearValidator());
        validatorMap.put("eyr", new ExpirationYearValidator());
        validatorMap.put("hgt", new HeightValidator());
        validatorMap.put("hcl", new HairValidator());
        validatorMap.put("ecl", new EyeValidator());
        validatorMap.put("pid", new PidValidator());
        
        String[] requiredFields = validatorMap.keySet().toArray(args);
        
        try
        {
            String filePath = AdventUtil.getPathTo("day4_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            Map<String, String> passport = null;
            while ((line = reader.readLine()) != null)
            {
                if (line.isEmpty())
                {
                    if (passport != null) {
                        passportList.add(passport);
                        passport = null;
                    }
                    continue;
                }
                
                if (passport == null)
                {
                    passport = new HashMap<String, String>();    
                }

                String[] fields = line.split(" ");
                for (int i = 0; i < fields.length; i++)
                {
                    String[] keyval = fields[i].trim().split(":");
                    passport.put(keyval[0].trim(), keyval[1].trim());
                }
            }
            
            if (passport != null) {
                passportList.add(passport);
                passport = null;
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int valid = 0;
        for (int i = 0; i < passportList.size(); i++)
        {
            boolean isValid = true;
            Map<String, String> passport = passportList.get(i);
            for (int j = 0; j < requiredFields.length; j++)
            {
                String passportField = requiredFields[j];
                if (passport.containsKey(passportField) == false)
                {
                    isValid = false;
                    break;
                }
                String passportFieldValue = passport.get(passportField);
                if (validatorMap.get(passportField).validate(passportFieldValue) == false)
                {
                    isValid = false;
                    break;
                }
            }
            if (isValid)
            {
                valid = valid + 1;
            }
        }
        
        System.out.println(valid);
                
    }

}
