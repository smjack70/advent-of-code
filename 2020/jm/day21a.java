import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class day21a {

    public static void main(String[] args) {

        Map<String, Set<String>> allergenMap = new HashMap< String, Set<String> >();
        Map<String, Integer> ingredientCounts = new HashMap<String, Integer>();
        try
        {
            String filePath = AdventUtil.getPathTo("day21_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null)
            {
                String[] parts = line.split(" [(]contains ");
                String[] ingredients = parts[0].trim().split(" "); 
                String[] allergens = parts[1].substring(0, parts[1].length() - 1).split(", ");
                
                for (String ingredient : ingredients)
                {
                    if (false == ingredientCounts.containsKey(ingredient))
                    {
                        ingredientCounts.put(ingredient, 0);
                    }
                    int currentCount = ingredientCounts.get(ingredient);
                    ingredientCounts.put(ingredient, currentCount + 1);
                }
                for (String allergen : allergens)
                {
                    if (allergenMap.containsKey(allergen) == true)
                    {
                        Set<String> possible = allergenMap.get(allergen);
                        possible.retainAll(Arrays.asList(ingredients));
                    }
                    else
                    {
                        Set<String> possible = new HashSet<String>(Arrays.asList(ingredients));
                        allergenMap.put(allergen, possible);
                    }
                }
                
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        Set<String> allAllergens = new HashSet<String>(); 
        
        // Identify each allergen
        boolean stop = true;
        do
        {
            stop = true;
            for (String allergen : allergenMap.keySet())
            {
                Set <String> possible = allergenMap.get(allergen);
                if (possible.size() > 1)
                {
                    stop = false;
                    for (String otherAllergen : allergenMap.keySet())
                    {
                        if (allergen.equals(otherAllergen))
                        {
                            continue;
                            
                        }
                        Set<String> otherPossible = allergenMap.get(otherAllergen);
                        if (otherPossible.size() > 1)
                        {
                            continue;
                        }
                        possible.removeAll(otherPossible);
                    }
                }
                else {
                    allAllergens.addAll(possible);
                }
            }
            
        } while (false == stop);
        
        int count = 0;
        for (String ingredient : ingredientCounts.keySet())
        {
            if (allAllergens.contains(ingredient))
            {
                continue;
            }
            count += ingredientCounts.get(ingredient);
        }
        System.out.println(count);
        
        
        
    } // end main

}
