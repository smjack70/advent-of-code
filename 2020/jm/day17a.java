import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class day17a {
    
    private static class Node
    {
        private int x = 0;
        private int y = 0;
        private int z = 0;
        private boolean active = false;
        private boolean flip = false;
        
        public Node(int x, int y, int z, boolean active)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.active = active;
        }
        
        public Node(int x, int y, int z, char activeState)
        {
            this.x = x;
            this.y = y;
            this.z = z;            
            this.active = (activeState == '#');
        }
        
        public int getX()
        {
            return this.x;
        }
        
        public int getY()
        {
            return this.y;
        }
        
        public int getZ()
        {
            return this.z;
        }
        
        public boolean isActive()
        {
            return this.active;
        }
        
        public void doFlip()
        {
            if (this.flip == true)
            {
                this.active = !this.active;
                this.flip = false;
            }
        }
        
        public void setFlip()
        {
            this.flip = true;
        }
    }
    
    
    private static class DimensionMap
    {
        private Map<Integer, Map<Integer, Map<Integer, Node>>> dimMap = null;
        private int xMax = 0;
        private int xMin = 0;
        private int yMax = 0;
        private int yMin = 0;
        private int zMax = 0;
        private int zMin = 0;
        
        public DimensionMap()
        {
            this.dimMap = new HashMap<Integer,  Map<Integer, Map<Integer, Node>>>();
        }
        
        public void addNode(Node node)
        {
            int x = node.getX();
            if (x > xMax)
            {
                xMax = x;
            }
            if (x < xMin)
            {
                xMin = x;
            }
            
            int y = node.getY();
            if (y > yMax)
            {
                yMax = y;
            }
            if (y < yMin)
            {
                yMin = y;
            }
            
            int z = node.getZ();
            if (z > zMax)
            {
                zMax = z;
            }
            if (z < zMin)
            {
                zMin = z;
            }
            
            Map<Integer, Map<Integer, Node>> xMap = null;
            Map<Integer, Node> yMap = null;
            if (!dimMap.containsKey(x))
            {
                xMap = new HashMap<Integer, Map<Integer, Node>>();
                dimMap.put(x, xMap);
            }
            else
            {
                xMap = dimMap.get(x);
            }
            
            if (!xMap.containsKey(y))
            {
                yMap = new HashMap<Integer, Node>();
                xMap.put(y, yMap);
            }
            else 
            {
                yMap = xMap.get(y); 
            }
            yMap.put(z, node);
        }
        
        public Node getNode(int x, int y, int z)
        {
            try
            {
                return dimMap.get(x).get(y).get(z);
            }
            catch (Exception e)
            {
                // pass
            }

            return null;
        }
        
        public int getNumNodes()
        {
            int count = 0;
            for (int x = xMin; x <= xMax; x++)
            {
                for (int y = yMin; y <= yMax; y++)
                {
                    for (int z = zMin; z <= zMax; z++)
                    {
                        if (null != getNode(x, y, z))
                        {
                            count = count + 1;
                        }
                    }
                }
            }
            return count;
        }
        
        public int getActiveNodeCount()
        {
            int count = 0;
            for (int x = xMin; x <= xMax; x++)
            {
                for (int y = yMin; y <= yMax; y++)
                {
                    for (int z = zMin; z <= zMax; z++)
                    {
                        Node node = getNode(x,y,z);
                        if (null != node && node.isActive())
                        {
                            count = count + 1;
                        }
                    }
                }
            }
            return count;
        }
        
        
        private void processFlips()
        {
            for (int x = xMin; x <= xMax; x++)
            {
                for (int y = yMin; y <= yMax; y++)
                {
                    for (int z = zMin; z <= zMax; z++)
                    {
                        Node node = getNode(x,y,z);
                        if (null != node)
                        {
                            node.doFlip();
                        }
                    }
                }
            }
        }
        
        private int getActiveNeighborCount(Node node)
        {
            int count = 0;
            int x = node.getX();
            int y = node.getY();
            int z = node.getZ();
            
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++ )
                    {
                        if (i == 0 && j == 0 && k == 0)
                        {
                            continue;
                        }
                        Node neighbor = getNode(x + i, y + j, z + k);
                        if ( (null != neighbor) && (neighbor.isActive()) )
                        {
                            count++;
                        }
                    }
                }
            }
            return count;
        }
        
        public void processStates()
        {
            int xHigh = xMax + 1;
            int xLow  = xMin - 1;
            int yHigh = yMax + 1;
            int yLow  = yMin - 1;
            int zHigh = zMax + 1;
            int zLow  = zMin - 1;
            
            for (int x = xLow; x <= xHigh; x++)
            {
                for (int y = yLow; y <= yHigh; y++)
                {
                    for (int z = zLow; z <= zHigh; z++)
                    {
                        Node node = getNode(x,y,z);
                        if (null == node)
                        {
                            node = new Node(x,y,z,false);
                            addNode(node);
                        }
                        int activeNeighbors = getActiveNeighborCount(node);
                        
                        if (node.isActive())
                        {
                            if (activeNeighbors < 2 || activeNeighbors > 3)
                            {
                                node.setFlip();
                            }
                        }
                        else if (activeNeighbors == 3) {
                            node.setFlip();
                        }
                            
                    }
                }
            }
            
            processFlips();

        }
    }
   
    public static void main(String[] args) {

        DimensionMap matrix = new DimensionMap();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day17_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            int y = 0;
            while ((line = reader.readLine()) != null)
            {
                for (int x = 0; x < line.length(); x++)
                {
                    char state = line.charAt(x);
                    Node node = new Node(x, y, 0, state);
                    matrix.addNode(node);
                }
                y = y + 1;
            }
            
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        for (int x = 0; x < 6; x++)
        {
            matrix.processStates();
        }
        
        System.out.println(matrix.getActiveNodeCount());
        
    }

}
