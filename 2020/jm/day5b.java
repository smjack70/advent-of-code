import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day5b {
    static char F = 'F'; // Binary 0
    static char B = 'B'; // Binary 1
    
    static char L = 'L'; // Binary 0
    static char R = 'R'; // Binary 1
    
    private static int getSeatNumber(String seatCode)
    {
        int row = 0;
        int column = 0;
        
        for (int i = 0; i < 7; i++)
        {
            row = row << 1;
            if (seatCode.charAt(i) == B) {
                row = row + 1;
            }
        }
        for (int i = 7; i < 10; i++)
        {
            column = column << 1;
            if (seatCode.charAt(i) == R)
            {
                column = column + 1;
            }
        }
        
        return (row * 8) + column;
    }

    
    public static void main(String[] args) {
        
        
        int highestSeat = 0;
        List<Integer> seatList = new ArrayList<Integer>();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day5_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            
            while ((line = reader.readLine()) != null)
            {
                int seatNumber = getSeatNumber(line);
                if(seatNumber > highestSeat)
                {
                    highestSeat = seatNumber;
                }
                seatList.add(seatNumber);
                
            }
            
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int columnMask = 0x3F8; // x3F8 is binary 1111111000, which should mask off the column value.
        int lastRow = highestSeat & columnMask; 
        
        for (int seat = 8; seat < lastRow; seat++)
        {
            if ( (seatList.contains(seat) ) == false && 
                 (seatList.contains(seat + 1) == true) && 
                 (seatList.contains(seat - 1) == true) )
            {
                System.out.println(seat);
            }
        }
                
    }

}
