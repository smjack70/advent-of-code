import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class day15 {

    public static void main(String[] args) {

        Map<Integer, List<Integer>> gameCounts  = new HashMap<Integer,List<Integer>>();
        List<Integer> initialRound = new ArrayList<Integer>();
        try
        {
            String filePath = AdventUtil.getPathTo("day15_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line[] = reader.readLine().split(",");
            for (String entry : line)
            {
                initialRound.add(Integer.parseInt(entry));
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int round = 1;
        //int maxRounds = 2020;
        int maxRounds = 30000000;
        int last = 0;
        for (int roundValue : initialRound)
        {
            if (gameCounts.containsKey(roundValue) == false)
            {
                List<Integer> roundsCalled = new ArrayList<Integer>();
                gameCounts.put(roundValue, roundsCalled);
            }
            List<Integer> roundsCalled = gameCounts.get(roundValue);
            roundsCalled.add(round);
            round = round + 1;
            last = roundValue;
        }
        
        while (round <= maxRounds)
        {
            int next = 0;
            
            List<Integer> roundsCalled = gameCounts.get(last);
            int listSize = roundsCalled.size();
            
            if (listSize > 1)
            {
                int x = roundsCalled.get(listSize - 1);
                int y = roundsCalled.get(listSize - 2);
                next = x - y;
            }
            if (gameCounts.containsKey(next) == false)
            {
                List<Integer> nextRounds = new ArrayList<Integer>();
                gameCounts.put(next, nextRounds);
            }
            roundsCalled = gameCounts.get(next);
            roundsCalled.add(round);
            last = next;
            round = round + 1;
        }
        
        System.out.println(last);
        
       
    }

}
