import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day1b {

    private static Integer TARGET_VALUE = 2020;
    
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        try
        {
            String filePath = AdventUtil.getPathTo("day1_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null)
            {
                list.add(Integer.parseInt(line));
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        for (int i = 0; i < list.size() - 2; i++)
        {
            for (int j = i + 1; j < list.size() - 1; j++ )
            {
                for (int k = j + 1; k < list.size(); k++)
                {
                    
                    int a = list.get(i);
                    int b = list.get(j);
                    int c = list.get(k);
                    if (a + b + c  == TARGET_VALUE)
                    {
                        System.out.println(a * b * c);
                    }
                }
            }
        }
    }

}
