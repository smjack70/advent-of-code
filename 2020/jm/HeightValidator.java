
public class HeightValidator implements ValidateData {

	String CM = "cm";
	String IN = "in";
	
	@Override
	public boolean validate(String data) throws Exception{

		int unitsOffs = data.length() - 2;
		String units = data.substring(unitsOffs);
		if ( !units.equals(CM) && !units.equals(IN) )
		{
			return false;
		}
		
		int  value = Integer.parseInt(data.substring(0, unitsOffs));
		if (units.equals(CM) && (value < 150 || value > 193 ) )
		{
			return false;
		}
		else if (units.equals(IN) && (value < 59 || value > 76) )
		{
			return false;
		}
						
		return true;
	}

}
