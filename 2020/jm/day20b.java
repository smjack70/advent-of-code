import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day20b {

    public enum Orientation
    {
        UP,
        RIGHT,
        DOWN,
        LEFT,
    }
    
    private static class Tile
    {   
        private long id = 0;
        
        private String northEdge = null;
        private String southEdge = null;
        private String eastEdge  = null;
        private String westEdge  = null;
        
        private List<String> tile = null;
        private Tile north = null;
        private Tile east = null;
        private Tile south = null;
        private Tile west = null;
        
        
        public Tile(int id, List<String> tile)
        {
            this.id = id;
            this.tile = new ArrayList<String>(tile);
            
            // Assemble strings that define the edges of the "tile". Top and 
            // bottom go left to right, east and west go top to bottom.
            StringBuilder builder = new StringBuilder();
            
            // Get the North edge pattern
            this.northEdge = new String (tile.get(0));
            // Get the South Edge pattern
            this.southEdge = new String(tile.get(tile.size() - 1));

            // get the West edge pattern
            for (int i = 0; i < tile.size(); i++)
            {
                builder.append(tile.get(i).charAt(0));
            }
            this.westEdge = builder.toString();
            
            builder.setLength(0);
            
            // get the East edge of the pattern
            for (int i = 0; i < tile.size(); i++)
            {
                String tileLine = tile.get(i);
                int endIndex = tileLine.length() - 1;
                builder.append(tileLine.charAt(endIndex));
            }
            this.eastEdge = builder.toString();
            builder.setLength(0);
            
            //System.out.println(this.id + " : N<" + this.northEdge +"> E<" + this.eastEdge + "> S<" + this.southEdge + "> W<" + this.westEdge +">");
        }
        
        public void rotateImageClockwise()
        {
            
            List<String> newTile = new ArrayList<String>();
            int sideLen = tile.size();
            for (int i = 0; i < sideLen; i++)
            {
                builder.setLength(0);
                for (int j = 0; j < sideLen; j++)
                {
                    String line = tile.get(j);
                    builder.append(line.charAt(i));
                }
                String newLine = builder.toString();
                newLine = reverseString(newLine);
                newTile.add(newLine);
            }
            tile.clear();
            tile = newTile;
        }
        
        public void flipImageHorizonal()
        {
            for (int i = 0; i < tile.size(); i++)
            {
                String line = tile.get(i);
                line = reverseString(line);
                tile.set(i, line);
            }
        }
        
        public void flipImageVertical()
        {
            String temp = null;
            for (int i = 0; i < tile.size() / 2; i ++)
            {
                int otherIndex = tile.size() - i -1;
                temp = tile.get(i);
                tile.set(i, tile.get(otherIndex));
                tile.set(otherIndex, temp);
            }
        }
        
        public long getId()
        {
            return this.id;
        }
        
        private static StringBuilder builder = new StringBuilder(); 
        
        public String reverseString(String value)
        {
            builder.setLength(0);
            builder.append(value);
            return builder.reverse().toString();
        }
        
        public void rotateClockwise()
        {
            //Save the North edge value
            String origNorth = northEdge;
            // Reverse the west edge, and make it the north edge
            northEdge = reverseString(westEdge);
            // Set the South edge to the west edge. It's fine as-is.
            westEdge = southEdge;
            // Reserve the east edge, and make it the south edge
            southEdge = reverseString(eastEdge);
            // Set the saved north edge to the east edge. It's fine as-is
            eastEdge = origNorth;
            rotateImageClockwise();
        }
        
        public void flipVertical()
        {
            String oldNorth = northEdge;
            northEdge = southEdge;
            southEdge = oldNorth;
            westEdge = reverseString(westEdge);
            eastEdge = reverseString(eastEdge);
            flipImageVertical();
        }
        
        public void flipHorizonal()
        {
            northEdge = reverseString(northEdge);
            southEdge = reverseString(southEdge);
            
            String oldWest = westEdge;
            westEdge = eastEdge;
            eastEdge = oldWest;
            flipImageHorizonal();
        }
        
        public boolean sharesEdgeWith(Tile tile)
        {
            for (int i = 0; i < 4; i++)
            {
                
                if (northEdge.equals(tile.southEdge) || southEdge.equals(tile.northEdge) || westEdge.equals(tile.eastEdge) || eastEdge.equals(tile.westEdge))
                {
                    return true;
                }
                tile.flipHorizonal();
                if (northEdge.equals(tile.southEdge) || southEdge.equals(tile.northEdge) || westEdge.equals(tile.eastEdge) || eastEdge.equals(tile.westEdge))
                {
                    return true;
                }
                tile.flipHorizonal();
                tile.flipVertical();
                if (northEdge.equals(tile.southEdge) || southEdge.equals(tile.northEdge) || westEdge.equals(tile.eastEdge) || eastEdge.equals(tile.westEdge))
                {
                    return true;
                }
                tile.flipVertical();
                tile.rotateClockwise();
            }
            return false;
        }
        
        public void print()
        {
            StringBuilder builder = new StringBuilder();
            System.out.println("Tile "+ this.getId());
            for (String line : tile)
            {
                System.out.println(line);
            }
            System.out.println("N<" + this.northEdge +">");
            System.out.println("E<" + this.eastEdge +">");
            System.out.println("S<" + this.southEdge +">");
            System.out.println("W<" + this.westEdge +">");
        }
        
    }
    

    public static void main(String[] args) {

        List<Tile> tiles = new ArrayList<Tile>();
        try
        {
            String filePath = AdventUtil.getPathTo("day20_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            int id = 0;
            List<String> tileLines = new ArrayList<String>();
            while ((line = reader.readLine()) != null)
            {
                if (line.isEmpty())
                {
                    tiles.add(new Tile(id, tileLines));
                    tileLines.clear();
                }
                else if (line.contains("Tile"))
                {
                    String[] parts = line.split(" ");
                    id = Integer.parseInt(parts[1].substring(0, parts[1].length() - 1));
                }
                else
                {
                    tileLines.add(line);
                }
                
            }
            
            if (tileLines.size() > 0)
            {
                tiles.add(new Tile(id, tileLines));
                tileLines.clear();
            }
            
            
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        long product = 1;
        for (Tile tile : tiles)
        {
            int edgeCount = 0;
            for (Tile otherTile : tiles)
            {
                if (otherTile == tile)
                {
                    continue;
                }
                if (tile.sharesEdgeWith(otherTile))
                {
                    edgeCount++;
                }
            }
            
            if (edgeCount == 2)
            {
                product = product * tile.getId();
            }
        }
        System.out.println(product);
        
    } // end main

}
