import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day16a {
    
    
    private static class Ticket
    {
        List<Integer> numbers;
        public Ticket(String values)
        {
            this.numbers = new ArrayList<Integer>();
            String[] nums = values.split(",");
            for (String i : nums)
            {
                numbers.add(Integer.parseInt(i));
            }
        }
        
        public int getSize()
        {
            return this.numbers.size();
        }
        
        public int getValueAt(int index)
        {
            return numbers.get(index).intValue();
        }
    }
    private static class TicketRule
    {
        
        private class RulePair
        {
            int high;
            int low;
            public RulePair(int low, int high)
            {
                this.low = low;
                this.high = high;
            }
            public int getLow()
            {
                return this.low;
            }
            public int getHigh()
            {
                return this.high;
            }
        }
        
        private List<RulePair> rules = null;
        private String label = null;
        public TicketRule(String label)
        {
            this.label = new String(label);
            this.rules = new ArrayList<RulePair>();
        }
        
        public String getLabel()
        {
            return this.label;
        }
        
        public void addRule(int low, int high)
        {
            RulePair rule = new RulePair(low, high);
            this.rules.add(rule);
        }
        
        public boolean validateValue(int value)
        {
            for (RulePair rule : rules)
            {
                if (value <= rule.getHigh() && value >= rule.getLow())
                {
                    return true;
                }
            }
            return false;
        }
        
    }

    public static void main(String[] args) {

        List <TicketRule> ticketRules = new ArrayList<TicketRule>();
        List<Ticket> tickets = new ArrayList<Ticket>();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day16_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            while ((line = reader.readLine()) != null)
            {
                if (line.isEmpty())
                {
                    break;
                }
                String[] elements = line.split(": ");
                TicketRule rule = new TicketRule(elements[0]);
                String[] rules = elements[1].split(" or ");
                for (String x : rules)
                {
                    String[] values = x.split("-");
                    int low = Integer.parseInt(values[0]);
                    int high = Integer.parseInt(values[1]);
                    rule.addRule(low, high);
                }
                ticketRules.add(rule);
            }
            
            // read my ticket
            reader.readLine(); // Read off the label
            tickets.add(new Ticket(reader.readLine()));
            reader.readLine(); // Read blank line
            
            // Read other tickets
            reader.readLine(); // Read off the label
            while (( line = reader.readLine()) != null)
            {
                tickets.add(new Ticket(line));
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int count = 0;
        for (int i = 1; i < tickets.size(); i++)
        {
            Ticket t = tickets.get(i);
            for (int j = 0; j < t.getSize(); j++)
            {
                int value = t.getValueAt(j);
                boolean valid = false;
                for (TicketRule rule : ticketRules)
                {
                    valid |= rule.validateValue(value);
                }
                if (false == valid)
                {
                    count += value;
                }
            }
        }
        System.out.println(count);
    }

}
