import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class day19b {
    
    public static enum RuleType
    {
        VALUE,
        REFERENCE,
        UNKNOWN
    };
    
    private static class Rule
    {
        private RuleType type = RuleType.UNKNOWN;
        private int key;
        
        private List<List<Integer>> references = new ArrayList<List<Integer>>();
        private char value = ' ';
        
        public Rule(String rule)
        {
            String[] parts = rule.split(":");
            this.key = Integer.parseInt(parts[0]);
            
            String part1 = parts[1].trim();
            
            if (part1.charAt(0) == '"')
            {
                this.value = part1.charAt(1);
                this.type = RuleType.VALUE;
            }
            else
            {
                String[] keySets = part1.split("[|]");
                for (String keySet : keySets)
                {
                    List<Integer> keyList = new ArrayList<Integer>();
                    String[] keys = keySet.trim().split(" ");
                    for (String key : keys)
                    {
                        int newKey = Integer.parseInt(key);
                        if (newKey == this.key)
                        {
                            keyList.add(null);
                        }
                        else {
                            keyList.add(newKey);
                        }
                    }
                    references.add(keyList);
                }
                this.type = RuleType.REFERENCE;
            }

        }
        
        public RuleType getRuleType()
        {
            return this.type;
        }
        
        public int getKey()
        {
            return this.key;
        }
        
        public List<List<Integer>> getReferences()
        {
            return this.references;
        }
        
        public char getValue()
        {
            return this.value;
        }
    }

    private static class RuleEngine
    {
        private Map<Integer, Rule> rules = null;
        private Map<Integer, List<Pattern>> matches = null;
        public RuleEngine()
        {
            this.rules = new HashMap<Integer, Rule>();
            this.matches = new HashMap<Integer, List<Pattern>>();
        }
        
        public void addRule(Rule rule)
        {
            rules.put(rule.getKey(), rule);
        }
        
        private void appendPatternString(List<String> patterns, String add)
        {
            for (int i = 0; i < patterns.size(); i++)
            {
                patterns.set(i, patterns.get(i) + add);
            }
        }
        
        private void appendPatternString(List<String> patterns, char add)
        {
            for (int i = 0; i < patterns.size(); i++)
            {
                patterns.set(i, patterns.get(i) + add);
            }
        }

        private List<String> assemblePatterns(List<Integer> keys, String pattern)
        {
            List<String> patterns = new ArrayList<String>();
            patterns.add(pattern);
            boolean loopPattern = false;
            boolean loopLast = false;
            
            if (keys.contains(null))
            {
                
                appendPatternString(patterns, '(');
                loopPattern = true;
                if (keys.get(keys.size() - 1) == null)
                {
                    loopLast = true;
                }
            }
            for (Integer key : keys)
            {
                if (key == null)
                {
                    if (loopLast)
                    {
                        appendPatternString(patterns, ")!");
                    }
                    else
                    {
                        appendPatternString(patterns, ")!(");
                    }
                    continue;
                }
                Rule rule = rules.get(key.intValue());
                switch (rule.getRuleType()) 
                {
                case VALUE:
                    char value = rule.getValue();
                    appendPatternString(patterns, value);
                    break;
                case REFERENCE:
                    List<String> newPatterns = new ArrayList<String>();
                    List<List<Integer>> keySets = rule.getReferences();
                    for (List<Integer> keySet : keySets)
                    {
                        for (String p : patterns)
                        {
                            newPatterns.addAll(assemblePatterns(keySet, p));
                        }
                    }
                    patterns = newPatterns;
                    break;
                default:
                    break;
                }
            }
            if (loopPattern == true && loopLast == false)
            {
                appendPatternString(patterns, ")!");
            }
            return patterns;
        }
        
        private List<String> assemblePatterns(int key, String pattern)
        {
            List<Integer> keys = new ArrayList<Integer>();
            keys.add(key);
            return assemblePatterns(keys, pattern);
        }
        
        private void buildRulePatterns(int key)
        {
            List<String> patterns = assemblePatterns(key, "");
            
            Set<String> first = new HashSet<String>();
            Set<String> second = new HashSet<String>();
            Set<String> third = new HashSet<String>();
            
            for (String pattern : patterns)
            {
                String[] x = pattern.split("!");
                first.add(x[0]);
                second.add(x[1]);
                third.add(x[2]);
            }
            
            String pattern = "[";
            for (String x : first)
            {
                pattern = pattern + x;
            }
            pattern = pattern +"]+[";
            for (String x : second)
            {
                pattern = pattern + x;
            }
            pattern = pattern +"]+[";
            for (String x : third)
            {
                pattern = pattern + x;
            }
            pattern = pattern +"]+";

            List<Pattern> regex = new ArrayList<Pattern>();
            Pattern x = Pattern.compile(pattern);
            regex.add(x);
            
            System.out.println(pattern);
            this.matches.put(key, regex);
        }
        
        
        public boolean matchRule(int key, String value)
        {
            if (!this.matches.containsKey(key))
            {
                buildRulePatterns(key);
            }
            List<Pattern> patterns = this.matches.get(key);
            for (Pattern pattern : patterns)
            {
                if (pattern.matcher(value).matches())
                {
                    System.out.println(pattern.toString());
                    System.out.println(value);
                    System.out.println("Match!");
                    return true;
                }
            }
            
            return false;
        }
        
    }

    public static void main(String[] args) {

        RuleEngine rules = new RuleEngine();
        List<String> patterns = new ArrayList<String>();
        try
        {
            //String filePath = AdventUtil.getPathTo("day19_input.txt");
            String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            while (!(line = reader.readLine()).isEmpty())
            {
                Rule rule = new Rule(line);
                rules.addRule(rule);
            }
            while ((line = reader.readLine()) != null)
            {
                patterns.add(line);
            }
            
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int matchCount = 0;
        
        /*
        Rule rule8  = new Rule("8: 42 | 42 8");
        Rule rule11 = new Rule("11: 42 31 | 42 11 31");
        */
        Rule rule8  = new Rule("8: 42 8");
        Rule rule11 = new Rule("11: 42 11 31");

        rules.addRule(rule8);
        rules.addRule(rule11);
        
        for (String pattern : patterns)
        {

            if ( rules.matchRule(0, pattern) == true)
            {
                matchCount++;
            }
        }
        System.out.println(matchCount);
        
    } // end main

}
