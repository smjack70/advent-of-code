import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class day16b {
    
    private static class TicketRule
    {
        
        private class RulePair
        {
            int high;
            int low;
            public RulePair(int low, int high)
            {
                this.low = low;
                this.high = high;
            }
            public int getLow()
            {
                return this.low;
            }
            public int getHigh()
            {
                return this.high;
            }
        }
        
        private List<RulePair> rules = null;
        private String label = null;
        public TicketRule(String label)
        {
            this.label = new String(label);
            this.rules = new ArrayList<RulePair>();
        }
        
        public String getLabel()
        {
            return this.label;
        }
        
        public void addRule(int low, int high)
        {
            RulePair rule = new RulePair(low, high);
            this.rules.add(rule);
        }
        
        public boolean validateValue(int value)
        {
            for (RulePair rule : rules)
            {
                if (value <= rule.getHigh() && value >= rule.getLow())
                {
                    return true;
                }
            }
            return false;
        }
        
    }
    
    private static class Ticket
    {
        List<Integer> numbers;
        public Ticket(String values)
        {
            this.numbers = new ArrayList<Integer>();
            String[] nums = values.split(",");
            for (String i : nums)
            {
                numbers.add(Integer.parseInt(i));
            }
        }
        
        public int getSize()
        {
            return this.numbers.size();
        }
        
        public int getValueAt(int index)
        {
            return numbers.get(index).intValue();
        }
        
        public boolean isValid(List<TicketRule> rules)
        {
            for (int value : numbers)
            {
                boolean isValid = false;
                for (TicketRule rule : rules)
                {
                    isValid = isValid | rule.validateValue(value);
                }
                if ( isValid == false) 
                {
                    return false;
                }
            }
            
            return true;
        }
    }
    
    private static Map<String, Integer> normalizePositionMap(Map<String, List<Integer>> positionMap)
    {
        Set<String> keys = positionMap.keySet();
        boolean normalized = true;
        Map<String, Integer> positions = new HashMap<String, Integer>();
        do
        {
            normalized = true;
            for (String key : keys)
            {
                List<Integer> intList = positionMap.get(key);
                if (intList.size() == 1)
                {
                    for (String rKey : keys)
                    {
                        if (rKey.equals(key))
                        {
                            continue;
                        }
                        positionMap.get(rKey).removeAll(intList);
                    }
                    positions.put(key,  intList.get(0));
                }
                else {
                    normalized = false;
                }
            }
            
        } while (!normalized);
        
        return positions;
        
    }
    
    private static void printPositionMap(Map<String, List<Integer>> positionMap)
    {
        for (String x : positionMap.keySet())
        {
            System.out.print(x + " : ");
            List<Integer> list = positionMap.get(x);
            for (int i : list)
            {
                System.out.print(i + " ");
            }
            System.out.println("");       
        }
    }
    

    public static void main(String[] args) {

        List <TicketRule> ticketRules = new ArrayList<TicketRule>();
        List<Ticket> tickets = new ArrayList<Ticket>();
        Ticket myTicket = null;
        
        try
        {
            String filePath = AdventUtil.getPathTo("day16_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            while ((line = reader.readLine()) != null)
            {
                if (line.isEmpty())
                {
                    break;
                }
                String[] elements = line.split(": ");
                TicketRule rule = new TicketRule(elements[0]);
                String[] rules = elements[1].split(" or ");
                for (String x : rules)
                {
                    String[] values = x.split("-");
                    int low = Integer.parseInt(values[0]);
                    int high = Integer.parseInt(values[1]);
                    rule.addRule(low, high);
                }
                ticketRules.add(rule);
            }
            
            // read my ticket
            reader.readLine(); // Read off the label
            myTicket = new Ticket(reader.readLine());
            reader.readLine(); // Read blank line
            
            // Read other tickets
            reader.readLine(); // Read off the label
            while (( line = reader.readLine()) != null)
            {
                tickets.add(new Ticket(line));
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        // Filter out the bad tickets
        List<Ticket> goodTickets = new ArrayList<Ticket>();
        for (Ticket ticket : tickets)
        {
            if ( ticket.isValid(ticketRules) )
            {
                goodTickets.add(ticket);
            }
        }

        // For each rule, get a list of position indexes where the rule is 
        // valid at those indexes for all good tickets.
        Map<String, List<Integer>> ruleMap = new HashMap<String, List<Integer>>();
        for (TicketRule rule : ticketRules)
        {
            List<Integer> positions = new ArrayList<Integer>();
            for (int i = 0; i < myTicket.getSize(); i++)
            {
                boolean valid = true;
                for (Ticket t : goodTickets)
                {
                    int value = t.getValueAt(i);
                    if (rule.validateValue(value) == false)
                    {
                        valid = false;
                        break;
                    }
                }
                if (valid == true)
                {
                    positions.add(i);
                }
            }
            ruleMap.put(rule.getLabel(), positions);
        }
        
        //printPositionMap(ruleMap);
        
        // Normalize the position rules: get it down to one position index for each rule.
        Map<String, Integer> indexMap = normalizePositionMap(ruleMap);
        
        // Multiply together the values from my ticket for fields starting with "departure"
        long count = 1;
        for (String key: indexMap.keySet())
        {
            if (key.matches("departure(.*)"))
            {
                int index = indexMap.get(key);
                count *= myTicket.getValueAt(index);
                
            }
        }
        
        System.out.println(count);
        
        
    }

}
