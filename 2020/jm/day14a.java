import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class day14a {

    private static final int  MASK_SIZE = 36;
    
    private static final char MASK_SKIP = 'X';
    private static final char MASK_ONE  = '1';
    private static final char MASK_ZERO = '0';
    
    
    private static long applyMask(long value, String mask)
    {
        String x = Long.toBinaryString(value);
        while (x.length() < MASK_SIZE)
        {
            x = "0" + x;
        }
        String bin= "";
        for (int i = 0; i < MASK_SIZE; i++)
        {
            switch (mask.charAt(i))
            {
            case MASK_SKIP:
                bin = bin + x.charAt(i);
                break;
            case MASK_ONE:
                bin = bin + MASK_ONE;
                break;
            case MASK_ZERO:
                bin = bin + MASK_ZERO;
                break;
            }
        }
        return Long.parseLong(bin, 2);
    }
    

    public static void main(String[] args) {

        List<String> commands = new ArrayList<String>();
        try
        {
            String filePath = AdventUtil.getPathTo("day14_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ( (line = reader.readLine()) != null)
            {
                commands.add(line);
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        // Initialize memory
        Map<Long, Long> memory = new HashMap<Long, Long>();

        // Initialize mask
        String mask = "";
        for (int i= 0; i < MASK_SIZE; i++)
        {
            mask = mask + MASK_SKIP;
        }
        
        for (String command : commands)
        {
            if (command.isEmpty())
            {
                continue;
            }
            String[] parts = command.split(" = ");
            if (parts[0].matches("mask"))
            {
                mask = new String(parts[1].trim());
            }
            else
            {
                long value = Long.parseLong(parts[1]);
                int x1 = parts[0].indexOf('[');
                int x2 = parts[0].indexOf(']');
                
                long addr = Long.parseLong(parts[0].substring(x1 + 1, x2));
                memory.put(addr, applyMask(value, mask) );
            }
        }
        
        Set<Long> addrs = memory.keySet();
        long value = 0;
        for (long addr : addrs)
        {
            value += memory.get(addr);
        }
        System.out.println(value);
    }

}
