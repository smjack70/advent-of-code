
public class ExpirationYearValidator implements ValidateData {

	@Override
	public boolean validate(String data) {
		if (data.length() != 4)
		{
			return false;
		}
		try
		{
			int year = Integer.parseInt(data);
			if ((year < 2020) || (year > 2030))
			{
				return false;
			}
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

}
