
public interface ValidateData {
	boolean validate(String data) throws Exception;
}
