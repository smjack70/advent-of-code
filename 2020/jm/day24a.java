import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class day24a {
    
    private static class Hex
    {
        private double ew = 0.0;
        private double ns = 0.0;
        public Hex(double ew, double ns)
        {
            this.ew = ew;
            this.ns = ns;
        }
        
        public boolean equals(Object o)
        {
            if (o == this)
            {
                return true;
            }
            
            if (!(o instanceof Hex))
            {
                return false;
            }
            
            Hex h = (Hex)o;
            if ( (h.ew == this.ew) && (h.ns == this.ns) )
            {
                return true;
            }
            
            return false;
        }
        
        public int hashCode()
        {
            return new Double(ew).hashCode() ^ new Double(ns).hashCode();
        }
    }
    
    private static void processVector(Map<Hex, Boolean> tiles, String vector)
    {
        double ns = 0.0;
        double ew = 0.0;
        int index = 0;
        while (index < vector.length())
        {
            char c = vector.charAt(index);
            switch (c)
            {
            case ('w'):
                ew -= 1.0;
                break;
            case ('e'):
                ew += 1.0;
                break;
            case ('n'):
                ns += 1.0;
                char x = vector.charAt(index +1);
                if (x == 'w')
                {
                    ew -= 0.5;
                }
                else 
                {
                    ew += 0.5;
                }
                index = index + 1;
                break;
            case ('s'):
                ns -= 1.0;
                char y = vector.charAt(index +1);
                if (y == 'w')
                {
                    ew -= 0.5;
                }
                else 
                {
                    ew += 0.5;
                }
                index = index + 1;
                break;
            }
            index++;
        }
        
        Hex newHex = new Hex(ew, ns);
        if (tiles.containsKey(newHex) == true) 
        {
            tiles.put(newHex, !tiles.get(newHex));
        }
        else
        {
            tiles.put(newHex, true);
        }
    }
    
    public static void main(String[] args) {

        List<String> vectors = new ArrayList<String>();
        Map<Hex, Boolean> hexTiles = new HashMap<Hex, Boolean>();
        try
        {
            String filePath = AdventUtil.getPathTo("day24_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null)
            {
                vectors.add(line);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        Hex start = new Hex(0.0, 0.0);
        hexTiles.put(start, false);
        for (String vector : vectors)
        {
            processVector(hexTiles, vector);
        }
        
        int count = 0;
        for (Map.Entry<Hex, Boolean> hex : hexTiles.entrySet())
        {
            if (hex.getValue() == true)
            {
                count++;
            }
        }
        System.out.println(count);
        
    } // end main

}
