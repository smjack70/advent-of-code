import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day3b {

    private static char TREE = '#';
    
    private static int checkSlope(List<List<Boolean>> slopeMap, int rightIncrem, int downIncrem)
    {
        int numTrees = 0;
        for (int right = 0, down = 0; down < slopeMap.size(); right = right + rightIncrem, down = down + downIncrem)
        {
            List<Boolean> track = slopeMap.get(down);
            if (track.size() <= right)
            {
                right = right - track.size();
            }
            if (track.get(right) == true)
            {
                numTrees++;
            }
            
        }
        return numTrees;
    }
    
    public static void main(String[] args) {
        List<List<Boolean>> slopeMap = new ArrayList<List<Boolean>>();

        try
        {
            String filePath = AdventUtil.getPathTo("day3_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null)
            {
                List<Boolean> trackPath = new ArrayList<Boolean>();
                for (int i = 0; i < line.length(); i++)
                {
                    if (line.charAt(i) == TREE)
                    {
                        trackPath.add(true);
                    }
                    else 
                    {
                        trackPath.add(false);
                    }
                }
                slopeMap.add(trackPath);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        long numTrees = 1;
        numTrees *= checkSlope(slopeMap, 1, 1);
        numTrees *= checkSlope(slopeMap, 3, 1);
        numTrees *= checkSlope(slopeMap, 5, 1);
        numTrees *= checkSlope(slopeMap, 7, 1);
        numTrees *= checkSlope(slopeMap, 1, 2);
        System.out.println(numTrees);
    }

}
