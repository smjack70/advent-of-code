import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day8a {
    
    enum InstructionType
    {
        NOP,
        ACC,
        JMP,
        UNK
    }
    
    private static class Instruction {
        private int value = 0;
        private InstructionType instr = InstructionType.UNK;
        private int callCount= 0;
        
        public Instruction(String type, int value)
        {
            this.value = value;
            if (type.equals("nop"))
            {
                this.instr = InstructionType.NOP;
            }
            else if (type.equals("acc"))
            {
                this.instr = InstructionType.ACC;
            }
            else if (type.equals("jmp"))
            {
                this.instr = InstructionType.JMP;
            }
            else
            {
                this.instr = InstructionType.UNK;
            }
        }
        
        public InstructionType getInstruction()
        {
            return this.instr;
        }
        
        public int getValue()
        {
            return this.value;
        }
        
        public void call()
        {
            this.callCount++;
        }
        public int getCallCount()
        {
            return this.callCount;            
        }
         
        
    }

    public static void main(String[] args) {
        
        List<Instruction> stack = new ArrayList<Instruction>();
        try
        {
            String filePath = AdventUtil.getPathTo("day8_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                String[] stackInstr = line.split(" ");
                String instr = stackInstr[0];
                int value = Integer.parseInt(stackInstr[1]);
                Instruction instrObj = new Instruction(instr, value);
                stack.add(instrObj);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int acc = 0;
        int nextInstr = 0;
        while (nextInstr < stack.size())
        {
            Instruction instr = stack.get(nextInstr);
            if (instr.getCallCount() > 0)
            {
                break;
            }
            instr.call();
            switch (instr.getInstruction())
            {
            case JMP:
                nextInstr += instr.getValue();
                break;
            case ACC:
                acc += instr.getValue();
                // Fall through intentionally to do the default one-up next instruction
            default:
                nextInstr += 1;
            }
        }
        System.out.println(acc);
                
    }

}
