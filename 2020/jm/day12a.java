import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day12a {

    
    private static final int NORTH = 0;
    private static final int EAST  = 90;
    private static final int SOUTH = 180;
    private static final int WEST  = 270;
    
    private static final int LEFT  = -1;
    private static final int RIGHT = 1;
    
    private static final char TURN_LEFT    = 'L';
    private static final char TURN_RIGHT   = 'R';
    private static final char MOVE_NORTH   = 'N';
    private static final char MOVE_EAST    = 'E';
    private static final char MOVE_SOUTH   = 'S';
    private static final char MOVE_WEST    = 'W';
    private static final char MOVE_FORWARD = 'F';
    
    private static class MyGps 
    {
        private int ewPosition = 0;
        private int nsPosition = 0;
        private int facing = EAST;
        
        public void doCommand(char command, int value)
        {
            switch (command)
            {
            case TURN_LEFT:
                turn(LEFT, value);
                break;
            case TURN_RIGHT:
                turn(RIGHT, value);
                break;
            case MOVE_NORTH:
                nsPosition += value;
                break;
            case MOVE_SOUTH:
                nsPosition -= value;
                break;
            case MOVE_EAST:
                ewPosition += value;
                break;
            case MOVE_WEST:
                ewPosition -= value;
                break;
            case MOVE_FORWARD:
                switch (facing)
                {
                case NORTH:
                    nsPosition += value;
                    break;
                case EAST:
                    ewPosition += value;
                    break;
                case SOUTH:
                    nsPosition -= value;
                    break;
                case WEST:
                    ewPosition -= value;
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }
        
        public void turn(int direction, int value)
        {
            int facing = this.facing + (direction * value);
            this.facing = facing % 360;
            if (this.facing < 0)
            {
                this.facing += 360;
            }
        }
        
        public int getNorthSouth()
        {
            return this.nsPosition;
        }
        
        public int getEastWest()
        {
            return this.ewPosition;
        }
        
        public int getFacing()
        {
            return this.facing;
        }
        
    }
    

    public static void main(String[] args) {
        
        List<String> commands = new ArrayList<String>();
        try
        {
            String filePath = AdventUtil.getPathTo("day12_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                commands.add(line);
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        MyGps gps = new MyGps();
        for (String line : commands)
        {
            char command = line.charAt(0);
            int value = Integer.parseInt(line.substring(1));
            gps.doCommand(command, value);
            //System.out.println(command + ":" + value + " ->(" + gps.getNorthSouth() + "," + gps.getEastWest() +") [" + gps.getFacing() +"]");
        }
                
        System.out.println(Math.abs(gps.getNorthSouth()) + Math.abs(gps.getEastWest()));
    }

}
