import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class day14b {

    private static final int  MASK_SIZE = 36;
    
    private static final char MASK_SKIP = 'X';
    private static final char MASK_ONE  = '1';
    private static final char MASK_ZERO = '0';
    
    private static void generateMasks(String addr, List<String> addrList)
    {
        if (addr.indexOf(MASK_SKIP) == -1)
        {
            addrList.add(addr);
            return;
        }
        
        String one = addr.replaceFirst("X", "1");
        generateMasks(one, addrList);
        String zero = addr.replaceFirst("X", "0");
        generateMasks(zero, addrList);
        return;
    }
    
    
    private static void applyMask(long value, long addr, String mask, Map<Long,Long> memory)
    {
        String addrStr = Long.toBinaryString(addr);
        char[] buffer = new char[MASK_SIZE];
        int offSet = MASK_SIZE - addrStr.length();
        for (int i = 0; i < offSet; i++)
        {
            buffer[i] = '0';
        }
        for (int i = 0; i < addrStr.length(); i ++)
        {
            buffer[i + offSet] = addrStr.charAt(i);
        }
        
        for (int i = 0; i < MASK_SIZE; i++)
        {
            switch (mask.charAt(i))
            {
            case MASK_SKIP:
                buffer[i] = MASK_SKIP;
                break;
            case MASK_ONE:
                buffer[i] = MASK_ONE;
                break;
            case MASK_ZERO:
                break;
            }
        }
        
        List<String> addrs = new ArrayList<String>();
        generateMasks(new String(buffer), addrs);
        for (String addrPattern : addrs)
        {
            long addrValue = Long.parseLong(addrPattern, 2);
            memory.put(addrValue, value);
        }
    }
    

    public static void main(String[] args) {

        List<String> commands = new ArrayList<String>();
        try
        {
            String filePath = AdventUtil.getPathTo("day14_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ( (line = reader.readLine()) != null)
            {
                commands.add(line);
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        // Initialize memory
        Map<Long, Long> memory = new HashMap<Long, Long>();

        // Initialize mask
        String mask = "";
        for (int i= 0; i < MASK_SIZE; i++)
        {
            mask = mask + MASK_SKIP;
        }
        
        for (String command : commands)
        {
            if (command.isEmpty())
            {
                continue;
            }
            String[] parts = command.split(" = ");
            if (parts[0].matches("mask"))
            {
                mask = new String(parts[1].trim());
            }
            else
            {
                long value = Long.parseLong(parts[1]);
                int x1 = parts[0].indexOf('[');
                int x2 = parts[0].indexOf(']');

                long addr = Long.parseLong(parts[0].substring(x1 + 1, x2));
                applyMask(value, addr, mask, memory);
            }
        }
        
        Set<Long> addrs = memory.keySet();
        long value = 0;
        for (long addr : addrs)
        {
            value += memory.get(addr);
        }
        System.out.println(value);
    }

}
