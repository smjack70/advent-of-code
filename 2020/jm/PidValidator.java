
public class PidValidator implements ValidateData {

	@Override
	public boolean validate(String data){
		return data.trim().matches("^[0-9]{9}$");
	}

}
