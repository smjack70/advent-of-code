import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class day6b {
    
    private static int getGroupOverlap(List<Set<Character>> groupForms)
    {
        if (groupForms.isEmpty() == true)
        {
            return 0;
        }
        
        // Since the overlap only counts when every set has each value, grab the 
        // first one and see where it overlaps with every other set. If there's
        // only one set, it overlaps with itself.
        Set<Character> first = groupForms.get(0);
        if (groupForms.size() == 1)
        {
            return first.size();
        }
        
        Iterator<Character> iter = first.iterator();
        int total = 0;
        while (iter.hasNext() == true)
        {
            boolean contains = true;
            char next = iter.next();
            for (int j = 1; j < groupForms.size(); j++)
            {
                if (groupForms.get(j).contains(next) == false)
                {
                    contains = false;
                    break;
                }
            }
            if (contains == true)
            {
                total = total + 1;
            }
        }
        
        return total;
    }
    

    public static void main(String[] args) {
        
        try
        {
            int total = 0;
            String filePath = AdventUtil.getPathTo("day6_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            List<Set<Character>> group = new ArrayList<Set<Character>>();
            while ((line = reader.readLine()) != null)
            {
                
                if (line.isEmpty() == true)
                {
                    total = total + getGroupOverlap(group);
                    group.clear();
                    continue;
                }

                Set<Character> form = new HashSet<Character>();
                for (int i = 0; i < line.length(); i++)
                {
                    form.add(line.charAt(i));
                }
                group.add(form);
            }
            
            total = total + getGroupOverlap(group);
            
            System.out.println(total);
            
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
                
    }

}
