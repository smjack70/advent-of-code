import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayDeque;
import java.util.Deque;

public class day22a {
    
    private static class Deck
    {
        private Deque<Integer> deck = null;
        
        public Deck()
        {
            deck = new ArrayDeque<Integer>();
        }
        
        public int size()
        {
            return deck.size();
        }
        
        public void add(int value)
        {
            deck.add(value);
        }
        
        public int draw()
        {
            return deck.pop();
        }
        
    }

    public static void main(String[] args) {

        Deck deck1 = new Deck();
        Deck deck2 = new Deck();
        try
        {
            String filePath = AdventUtil.getPathTo("day22_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            reader.readLine(); // Read off the player line
            while ((line=reader.readLine()).isEmpty() == false)
            {
                deck1.add(Integer.parseInt(line));
            }
            
            reader.readLine(); // Read off the player line
            while ((line = reader.readLine()) != null)
            {
                deck2.add(Integer.parseInt(line));
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        while ( (deck1.size() > 0)  && (deck2.size() > 0) )
        {
            int d1 = deck1.draw();
            int d2 = deck2.draw();
            if (d1 > d2)
            {
                deck1.add(d1);
                deck1.add(d2);
            }
            else
            {
                deck2.add(d2);
                deck2.add(d1);
            }
        }
        
        Deck winner = null;
        int deckSize = deck1.size();
        if (deckSize > 0)
        {
            winner = deck1;
        }
        else
        {
            deckSize = deck2.size();
            winner = deck2;
        }
        
        int value = 0;
        for (int i = deckSize; i > 0; i --)
        {
            value += (winner.draw() * i);
        }
        
        System.out.println(value);
        
        
    } // end main

}
