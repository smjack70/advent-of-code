import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class day24b {
    
    private static class Hex
    {
        private double ew = 0.0;
        private double ns = 0.0;
        public Hex(double ew, double ns)
        {
            this.ew = ew;
            this.ns = ns;
        }
        
        public Hex(Hex copy)
        {
            this.ew = copy.ew;
            this.ns = copy.ns;
        }
        
        public boolean equals(Object o)
        {
            if (o == this)
            {
                return true;
            }
            
            if (!(o instanceof Hex))
            {
                return false;
            }
            
            Hex h = (Hex)o;
            if ( (h.ew == this.ew) && (h.ns == this.ns) )
            {
                return true;
            }
            
            return false;
        }
        
        public int hashCode()
        {
            return new Double(ew).hashCode() ^ new Double(ns).hashCode();
        }
    }
    
    private static void addNeighbors(Map<Hex, Boolean> tiles, Hex hex)
    {
        Hex pointer = new Hex(0.0, 0.0);
        // North-east
        pointer.ew = hex.ew + 0.5;
        pointer.ns = hex.ns + 1.0;
        if (false == tiles.containsKey(pointer)) 
        {
            tiles.put(new Hex(pointer), false);
        }
        // North-west
        pointer.ew = hex.ew - 0.5;
        pointer.ns = hex.ns + 1.0;
        if (false == tiles.containsKey(pointer)) 
        {
            tiles.put(new Hex(pointer), false);
        }
        // South-east
        pointer.ew = hex.ew + 0.5;
        pointer.ns = hex.ns - 1.0;
        if (false == tiles.containsKey(pointer)) 
        {
            tiles.put(new Hex(pointer), false);
        }
        // South-west
        pointer.ew = hex.ew - 0.5;
        pointer.ns = hex.ns - 1.0;
        if (false == tiles.containsKey(pointer)) 
        {
            tiles.put(new Hex(pointer), false);
        }
        // East
        pointer.ew = hex.ew + 1.0;
        pointer.ns = hex.ns;
        if (false == tiles.containsKey(pointer)) 
        {
            tiles.put(new Hex(pointer), false);
        }
        // West
        pointer.ew = hex.ew - 1.0;
        pointer.ns = hex.ns;
        if (false == tiles.containsKey(pointer)) 
        {
            tiles.put(new Hex(pointer), false);
        }
    }
    
    private static void processVector(Map<Hex, Boolean> tiles, String vector)
    {
        double ns = 0.0;
        double ew = 0.0;
        int index = 0;
        while (index < vector.length())
        {
            char c = vector.charAt(index);
            switch (c)
            {
            case ('w'):
                ew -= 1.0;
                break;
            case ('e'):
                ew += 1.0;
                break;
            case ('n'):
                ns += 1.0;
                char x = vector.charAt(index +1);
                if (x == 'w')
                {
                    ew -= 0.5;
                }
                else 
                {
                    ew += 0.5;
                }
                index = index + 1;
                break;
            case ('s'):
                ns -= 1.0;
                char y = vector.charAt(index +1);
                if (y == 'w')
                {
                    ew -= 0.5;
                }
                else 
                {
                    ew += 0.5;
                }
                index = index + 1;
                break;
            }
            index++;
        }
        
        Hex newHex = new Hex(ew, ns);
        if (tiles.containsKey(newHex) == true) 
        {
            tiles.put(newHex, !tiles.get(newHex));
        }
        else
        {
            tiles.put(newHex, true);
        }
        addNeighbors(tiles, newHex);
        
    }
    
    public static int countBlackNeighbors(Map<Hex, Boolean> tileMap, Hex tile)
    {
        int count = 0;
        Hex pointer = new Hex(0.0, 0.0);
        // North-east
        pointer.ew = tile.ew + 0.5;
        pointer.ns = tile.ns + 1.0;
        if (tileMap.containsKey(pointer) && tileMap.get(pointer) == true)
        {
            count++;
        }
        // North-west
        pointer.ew = tile.ew - 0.5;
        pointer.ns = tile.ns + 1.0;
        if (tileMap.containsKey(pointer) && tileMap.get(pointer) == true)
        {
            count++;
        }
        // East
        pointer.ew = tile.ew + 1.0;
        pointer.ns = tile.ns;
        if (tileMap.containsKey(pointer) && tileMap.get(pointer) == true)
        {
            count++;
        }
        // West
        pointer.ew = tile.ew - 1.0;
        pointer.ns = tile.ns;
        if (tileMap.containsKey(pointer) && tileMap.get(pointer) == true)
        {
            count++;
        }
        // South-east
        pointer.ew = tile.ew + 0.5;
        pointer.ns = tile.ns - 1.0;
        if (tileMap.containsKey(pointer) && tileMap.get(pointer) == true)
        {
            count++;
        }
        // South-west
        pointer.ew = tile.ew - 0.5;
        pointer.ns = tile.ns - 1.0;
        if (tileMap.containsKey(pointer) && tileMap.get(pointer) == true)
        {
            count++;
        }
        return count;
    }
    
    private static Set<Hex> flipList = new HashSet<Hex>();
    public static void doTileFlip(Map<Hex, Boolean> tileMap)
    {
        flipList.clear();

        for (Map.Entry<Hex, Boolean> hex : tileMap.entrySet())
        {
            
            Hex current = hex.getKey();
            int count = countBlackNeighbors(tileMap, current);
            if (hex.getValue() == true) // Current tile is black
            {
                if ( (count == 0) || (count > 2) )
                {
                    flipList.add(current);
                }
            }
            else // current tile is white
            {
                if (count == 2)
                {
                    flipList.add(current);
                }
            }
        }
        
        for (Hex hex : flipList)
        {
            tileMap.put(hex, !tileMap.get(hex));
            addNeighbors(tileMap, hex );
        }
        
        flipList.clear();
    }

    
    private static int getBlackCount(Map<Hex, Boolean> tileMap)
    {
        int count = 0;
        for (Map.Entry<Hex, Boolean> hex : tileMap.entrySet())
        {
            if (hex.getValue() == true)
            {
                count++;
            }
        }

        return count;
    }
    
    public static void main(String[] args) {

        List<String> vectors = new ArrayList<String>();
        Map<Hex, Boolean> hexTiles = new HashMap<Hex, Boolean>();
        try
        {
            String filePath = AdventUtil.getPathTo("day24_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null)
            {
                vectors.add(line);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        Hex start = new Hex(0.0, 0.0);
        hexTiles.put(start, false);
        for (String vector : vectors)
        {
            processVector(hexTiles, vector);
        }
        
        for (int i = 0; i < 100; i++)
        {
            doTileFlip(hexTiles);
        }

        
        int count = getBlackCount(hexTiles);
        System.out.println(count);
        
    } // end main

}
