import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class day17b {
    
    private static class Node
    {
        private int x = 0;
        private int y = 0;
        private int z = 0;
        private int w = 0;
        private boolean active = false;
        private boolean flip = false;
        
        public Node(int x, int y, int z, int w, boolean active)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
            this.active = active;
        }
        
        public Node(int x, int y, int z, int w, char activeState)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
            this.active = (activeState == '#');
        }
        
        public int getX()
        {
            return this.x;
        }
        
        public int getY()
        {
            return this.y;
        }
        
        public int getZ()
        {
            return this.z;
        }
        
        public int getW()
        {
            return this.w;
        }
        
        public boolean isActive()
        {
            return this.active;
        }
        
        public void doFlip()
        {
            if (this.flip == true)
            {
                this.active = !this.active;
                this.flip = false;
            }
        }
        
        public void setFlip()
        {
            this.flip = true;
        }
    }
    
    
    private static class DimensionMap
    {
        private Map<Integer, Map<Integer, Map<Integer, Map<Integer, Node>>>> dimMap = null;
        private int xMax = 0;
        private int xMin = 0;
        private int yMax = 0;
        private int yMin = 0;
        private int zMax = 0;
        private int zMin = 0;
        private int wMax = 0;
        private int wMin = 0;
        
        public DimensionMap()
        {
            this.dimMap = new HashMap<Integer, Map<Integer,  Map<Integer, Map<Integer, Node>>>>();
        }
        
        public void addNode(Node node)
        {
            int x = node.getX();
            if (x > xMax)
            {
                xMax = x;
            }
            if (x < xMin)
            {
                xMin = x;
            }
            
            int y = node.getY();
            if (y > yMax)
            {
                yMax = y;
            }
            if (y < yMin)
            {
                yMin = y;
            }
            
            int z = node.getZ();
            if (z > zMax)
            {
                zMax = z;
            }
            if (z < zMin)
            {
                zMin = z;
            }
            
            int w = node.getW();
            if (w > wMax)
            {
                wMax = w;
            }
            if (w < wMin)
            {
                wMin = w;
            }
            
            Map<Integer, Map<Integer, Map<Integer, Node>>> xMap = null;
            Map<Integer, Map<Integer, Node>> yMap = null;
            Map<Integer, Node> zMap = null;
            if (!dimMap.containsKey(x))
            {
                xMap = new HashMap<Integer, Map<Integer, Map<Integer, Node>>>();
                dimMap.put(x, xMap);
            }
            else
            {
                xMap = dimMap.get(x);
            }
            
            if (!xMap.containsKey(y))
            {
                yMap = new HashMap<Integer, Map<Integer, Node>>();
                xMap.put(y, yMap);
            }
            else 
            {
                yMap = xMap.get(y); 
            }
            
            if (!yMap.containsKey(z))
            {
                zMap = new HashMap<Integer, Node>();
                yMap.put(z,  zMap);
            }
            else
            {
                zMap = yMap.get(z);
            }
            zMap.put(w, node);
        }
        
        public Node getNode(int x, int y, int z, int w)
        {
            try
            {
                return dimMap.get(x).get(y).get(z).get(w);
            }
            catch (Exception e)
            {
                // pass
            }

            return null;
        }
        
        public int getNumNodes()
        {
            int count = 0;
            for (int x = xMin; x <= xMax; x++)
            {
                for (int y = yMin; y <= yMax; y++)
                {
                    for (int z = zMin; z <= zMax; z++)
                    {
                        for (int w = wMin; w <= wMax; w++)
                        {
                            if (null != getNode(x, y, z, w))
                            {
                                count = count + 1;
                            }                            
                        }
                    }
                }
            }
            return count;
        }
        
        public int getActiveNodeCount()
        {
            int count = 0;
            for (int x = xMin; x <= xMax; x++)
            {
                for (int y = yMin; y <= yMax; y++)
                {
                    for (int z = zMin; z <= zMax; z++)
                    {
                        for (int w = wMin; w <= wMax; w++)
                        {
                            Node node = getNode(x,y,z,w);
                            if (null != node && node.isActive())
                            {
                                count = count + 1;
                            }
                        }
                    }
                }
            }
            return count;
        }
        
        
        private void processFlips()
        {
            for (int x = xMin; x <= xMax; x++)
            {
                for (int y = yMin; y <= yMax; y++)
                {
                    for (int z = zMin; z <= zMax; z++)
                    {
                        for (int w = wMin; w <= wMax; w++)
                        {
                            Node node = getNode(x,y,z,w);
                            if (null != node)
                            {
                                node.doFlip();
                            }    
                        }
                    }
                }
            }
        }
        
        private int getActiveNeighborCount(Node node)
        {
            int count = 0;
            int x = node.getX();
            int y = node.getY();
            int z = node.getZ();
            int w = node.getW();
            
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++ )
                    {
                        for (int m = -1; m <=1; m++)
                        {
                            if (i == 0 && j == 0 && k == 0 && m == 0)
                            {
                                continue;
                            }
                            Node neighbor = getNode(x + i, y + j, z + k, w + m);
                            if ( (null != neighbor) && (neighbor.isActive()) )
                            {
                                count++;
                            }    
                        }
                    }
                }
            }
            return count;
        }
        
        public void processStates()
        {
            int xHigh = xMax + 1;
            int xLow  = xMin - 1;
            int yHigh = yMax + 1;
            int yLow  = yMin - 1;
            int zHigh = zMax + 1;
            int zLow  = zMin - 1;
            int wHigh = wMax + 1;
            int wLow  = wMin - 1; 
            
            for (int x = xLow; x <= xHigh; x++)
            {
                for (int y = yLow; y <= yHigh; y++)
                {
                    for (int z = zLow; z <= zHigh; z++)
                    {
                        for (int w = wLow; w <= wHigh; w++)
                        {
                            Node node = getNode(x,y,z, w);
                            if (null == node)
                            {
                                node = new Node(x,y,z,w, false);
                                addNode(node);
                            }
                            int activeNeighbors = getActiveNeighborCount(node);
                            
                            if (node.isActive())
                            {
                                if (activeNeighbors < 2 || activeNeighbors > 3)
                                {
                                    node.setFlip();
                                }
                            }
                            else if (activeNeighbors == 3) {
                                node.setFlip();
                            }    
                        }
                    }
                }
            }
            
            processFlips();

        }
        
        public void print()
        {
            for (int z = zMin; z <= zMax; z++)
            {
                for (int w = wMin; w <= wMax; w++)
                {
                    System.out.println("z=" + z +", w=" + w);
                    for (int y = yMin; y <= yMax; y++)
                    {
                        for (int x = xMin; x <= xMax; x++)
                        {
                            if (getNode(x, y, z, w).isActive())
                            {
                                System.out.print('#');
                            }
                            else
                            {
                                System.out.print('.');
                            }
                        }
                        System.out.println("");
                    }
                }
            }

        }
        
    }
   
    public static void main(String[] args) {

        DimensionMap matrix = new DimensionMap();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day17_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            int y = 0;
            while ((line = reader.readLine()) != null)
            {
                for (int x = 0; x < line.length(); x++)
                {
                    char state = line.charAt(x);
                    Node node = new Node(x, y, 0, 0, state);
                    matrix.addNode(node);
                }
                y = y + 1;
            }
            
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        for (int x = 0; x < 6; x++)
        {
            matrix.processStates();
        }
        System.out.println(matrix.getActiveNodeCount());
        
    }

}
