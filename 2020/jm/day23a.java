import java.io.BufferedReader;
import java.io.FileReader;

public class day23a {
    
    private static class CircleNode
    {
        int value = 0;
        private CircleNode next = null;
        private CircleNode previous = null;
        
        public CircleNode(int value)
        {
            this.value = value;
            this.next = this;
            this.previous = next;
        }
        
        public void addAfter(CircleNode item)
        {
            item.next = this.next;
            item.previous = this;
            item.next.previous = item;
            this.next = item;
        }
        
        public CircleNode removeNextNode()
        {
            CircleNode remove = this.next;
            this.next = remove.next;
            remove.next.previous = this;
            remove.next = null;
            remove.previous = null;
            return remove;
        }
        
        public CircleNode getNext()
        {
            return next;
        }
        
        public CircleNode getPrevious()
        {
            return previous;
        }
        
        public int getValue()
        {
            return value;
        }
        
    }
    
    private static void insertNodes(CircleNode current, CircleNode c1, CircleNode c2, CircleNode c3)
    {
        CircleNode pointer = current;
        int val = current.getValue() -1;
        while (true)
        {
            pointer = pointer.getNext();
            if (pointer == current)
            {
                val = val -1;
                if (0 >= val)
                {
                    val = 9;
                }
            }
            else if (pointer.getValue() == val)
            {
                pointer.addAfter(c3);
                pointer.addAfter(c2);
                pointer.addAfter(c1);
                break;
            }
        }
    }
    
    private static void printCircle(CircleNode start)
    {
        CircleNode pointer = start;
        do
        {
            System.out.print(pointer.getValue());
            pointer = pointer.getNext();
        }
        while (pointer != start);
        System.out.println("");
            
    }

    public static void main(String[] args) {

        CircleNode current = null;
        try
        {
            String filePath = AdventUtil.getPathTo("day23_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            line = reader.readLine(); // Read off the player line
            for (char x : line.toCharArray())
            {
                CircleNode nextItem = new CircleNode(Character.getNumericValue(x));
                if (null == current)
                {
                    current = nextItem;
                }
                else
                {
                    current.previous.addAfter(nextItem);
                }
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        for (int i = 0; i < 100; i++)
        {
            CircleNode c1 = current.removeNextNode();
            CircleNode c2 = current.removeNextNode();
            CircleNode c3 = current.removeNextNode();
            insertNodes(current, c1, c2, c3);
            current = current.getNext();
        }
        
        while (current.getValue() != 1)
        {
            current = current.getNext();
        }
        printCircle(current);
        
        
        
    } // end main

}
