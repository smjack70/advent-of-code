import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day8b {
    
    enum InstructionType
    {
        NOP,
        ACC,
        JMP,
        UNK
    }
    
    private static class Instruction {
        private int value = 0;
        private InstructionType instr = InstructionType.UNK;
        private int callCount= 0;
        
        public Instruction(String type, int value)
        {
            this.value = value;
            if (type.equals("nop"))
            {
                this.instr = InstructionType.NOP;
            }
            else if (type.equals("acc"))
            {
                this.instr = InstructionType.ACC;
            }
            else if (type.equals("jmp"))
            {
                this.instr = InstructionType.JMP;
            }
            else
            {
                this.instr = InstructionType.UNK;
            }
        }
        
        public InstructionType getInstruction()
        {
            return this.instr;
        }
        
        public int getValue()
        {
            return this.value;
        }
        
        public void call()
        {
            this.callCount++;
        }
        public int getCallCount()
        {
            return this.callCount;            
        }
        
        public void resetCallCount()
        {
            this.callCount = 0;
        }
         
    }
    
    private static int getNextNopOrJump(int start, List<Instruction> stack)
    {
        for (int i = start; i < stack.size(); i++)
        {
            Instruction next = stack.get(i);
            InstructionType type = next.getInstruction();
            if (type == InstructionType.JMP || type == InstructionType.NOP)
            {
                return i;
            }
        }
        return -1;
    }
    
    private static void resetInstrCounts(List<Instruction> stack)
    {
        for (Instruction instr : stack)
        {
            instr.resetCallCount();
        }
    }

    public static void main(String[] args) {
        
        List<Instruction> stack = new ArrayList<Instruction>();
        try
        {
            String filePath = AdventUtil.getPathTo("day8_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                String[] stackInstr = line.split(" ");
                String instr = stackInstr[0];
                int value = Integer.parseInt(stackInstr[1]);
                Instruction instrObj = new Instruction(instr, value);
                stack.add(instrObj);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int acc = 0;
        int nextInstr = 0;
        int replace = getNextNopOrJump(0, stack);
        while (nextInstr < stack.size())
        {
            Instruction instr = stack.get(nextInstr);
            InstructionType instrType = instr.getInstruction();
            
            if (nextInstr == replace)
            {
                if (instrType == InstructionType.JMP)
                {
                    instrType = InstructionType.NOP;
                }
                else if (instrType == InstructionType.NOP)
                {
                    instrType = InstructionType.JMP;
                }
            }
            
            instr.call();
            switch (instrType)
            {
            case JMP:
                int jmpDest = nextInstr + instr.getValue();
                // If we've hit the same jump twice, we're in a loop. If we're 
                // outside the bounds of the instructions, we're toast. Either
                // way, reset and start the next iteration
                if (instr.getCallCount() > 1)
                {
                    nextInstr = 0;
                    acc = 0;
                    replace = getNextNopOrJump(replace + 1, stack);
                    resetInstrCounts(stack);
                    break;
                }
                nextInstr = jmpDest;
                break;
            case ACC:
                acc += instr.getValue();
                // Fall through intentionally to do the default one-up next instruction
            default:
                nextInstr += 1;
            }
        }
        System.out.println(acc);
                
    }

}
