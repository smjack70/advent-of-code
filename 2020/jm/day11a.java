import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day11a {
    
    private static final char FLOOR    = '.';
    private static final char OPEN     = 'L';
    private static final char OCCUPIED = '#';
    
    private static class HashedGrid
    {
        private Character[][] grid = null;
        HashedGrid(int rows, int columns)
        {
            this.grid = new Character[rows][columns];
        }
        
        HashedGrid(HashedGrid copy)
        {
            int rows = copy.getNumRows();
            int cols = copy.getNumColumns();
            this.grid = new Character[rows][cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    this.grid[i][j] = copy.getValue(i, j);
                }
            }
        }
        
        public void setValue(Character value, int row, int col)
        {
            this.grid[row][col] = value;
        }
        
        public char getValue(int row, int col)
        {
            return this.grid[row][col];
        }
        
        public int hashCode()
        {
            StringBuffer buffer = new StringBuffer();
            for (int row = 0; row < grid.length; row++)
            {
                for (int col = 0; col < grid[row].length; col++)
                {
                    buffer.append(grid[row][col]);
                }
            }
            return buffer.toString().hashCode();
        }
        
        public int getNumRows()
        {
            return grid.length;
        }
        
        public int getNumColumns()
        {
            return grid[0].length; 
        }
        
        public void printGrid()
        {
            for (int i = 0; i < this.getNumRows(); i++)
            {
                for (int j = 0; j < this.getNumColumns(); j++)
                {
                    System.out.print(this.getValue(i, j));
                }
                System.out.println("");
            }
        }
        
    }
    
    private static boolean isOccupied(HashedGrid grid, int x, int y)
    {
        if ( x < 0 || y < 0 || x >= grid.getNumRows() || y >= grid.getNumColumns())
        {
            return false;
        }
        return grid.getValue(x, y) == OCCUPIED;

    }
    
    private static void updateSeats(HashedGrid seats)
    {
        HashedGrid copy = new HashedGrid(seats);
        int rows = copy.getNumRows();
        int cols = copy.getNumColumns();
        
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                char val = copy.getValue(i, j);
                if (val == FLOOR)
                {
                    continue;
                }
                int occupied = 0;
                int x = i - 1;
                int y = j - 1;
                //NorthWest
                if ( isOccupied(copy, x,y) )
                {
                    occupied++;
                }
                // North
                y = j;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                // NorthEast
                y = j + 1;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                // West
                x = i;
                y = j - 1;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                // East
                x = i;
                y = j + 1;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                // SouthWest
                x = i + 1;
                y = j - 1;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                // South
                y = j;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                // SouthEast
                y = j + 1;
                if (isOccupied(copy, x,y))
                {
                    occupied++;
                }
                
                switch(val)
                {
                case OCCUPIED:
                    if (occupied >= 4)
                    {
                        seats.setValue(OPEN, i, j);
                    }
                    break;
                case OPEN:
                    if (occupied == 0)
                    {
                        seats.setValue(OCCUPIED, i, j);
                    }
                    break;
                default:
                    break;
                }
                
            }
        }
        
        
    }

    public static void main(String[] args) {
        
        
        HashedGrid seatGrid = null;
        
        try
        {
            String filePath = AdventUtil.getPathTo("day11_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            List<List<Character>> seats = new ArrayList<List<Character>>();
            while ((line = reader.readLine()) != null)
            {
                List<Character>floorLine = new ArrayList<Character>();
                for (int i = 0; i < line.length(); i++)
                {
                    floorLine.add(line.charAt(i));
                }
                
                seats.add(floorLine);
            }
            reader.close();
            
            int rows = seats.size();
            for (int i = 0; i < rows; i++)
            {
                List<Character> row = seats.get(i);
                int cols = row.size();
                if (seatGrid == null)
                {
                    seatGrid = new HashedGrid(rows, cols);
                }
                for (int j = 0; j < cols; j++)
                {
                    seatGrid.setValue(row.get(j), i, j);
                }
            }
                
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        
        int hashValue = 0;
        do
        {
            hashValue = seatGrid.hashCode();
            updateSeats(seatGrid);
        } while (hashValue != seatGrid.hashCode());
        
        int occupiedSeats = 0;
        for (int i = 0; i < seatGrid.getNumRows(); i++)
        {
            for (int j = 0; j < seatGrid.getNumColumns(); j++)
            {
                if (seatGrid.getValue(i, j) == OCCUPIED)
                {
                    occupiedSeats++;
                }
            }
        }
        System.out.println(occupiedSeats);

                
    }

}
