import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day12b {

    
    private static final int NORTH = 0;
    private static final int EAST  = 90;
    private static final int SOUTH = 180;
    private static final int WEST  = 270;
    
    private static final int LEFT  = -1;
    private static final int RIGHT = 1;
    
    private static final char TURN_LEFT    = 'L';
    private static final char TURN_RIGHT   = 'R';
    private static final char MOVE_NORTH   = 'N';
    private static final char MOVE_EAST    = 'E';
    private static final char MOVE_SOUTH   = 'S';
    private static final char MOVE_WEST    = 'W';
    private static final char MOVE_FORWARD = 'F';
    
    private static class MyGps 
    {
        private int ewPosition = 0;
        private int nsPosition = 0;
        private int wayPointNS = 1;
        private int wayPointEW = 10;
        
        public void doCommand(char command, int value)
        {
            switch (command)
            {
            case TURN_LEFT:
                rotateWaypoint(LEFT, value);
                break;
            case TURN_RIGHT:
                rotateWaypoint(RIGHT, value);
                break;
            case MOVE_NORTH:
                wayPointNS += value;
                break;
            case MOVE_SOUTH:
                wayPointNS -= value;
                break;
            case MOVE_EAST:
                wayPointEW += value;
                break;
            case MOVE_WEST:
                wayPointEW -= value;
                break;
            case MOVE_FORWARD:
                for (int i = 0; i < value; i++)
                {
                    ewPosition += wayPointEW;
                    nsPosition += wayPointNS;
                }
                break;
            default:
                break;
            }
        }
        
        private void rotateWaypoint(int direction, int value)
        {
            int quarters = value / 90;
            for (int i = 0; i < quarters; i++)
            {
                int newNS = 0;
                int newEW = 0;
                switch (direction)
                {
                case RIGHT:
                    newNS = -1 * wayPointEW;
                    newEW = wayPointNS;
                    break;
                case LEFT:
                    newNS = wayPointEW;
                    newEW = -1 *wayPointNS;
                    break;
                default:
                    System.out.println("How the hell did we get here?");
                    break;
                }
                wayPointNS = newNS;
                wayPointEW = newEW;
                
            }
        }
        
        public int getNorthSouth()
        {
            return this.nsPosition;
        }
        
        public int getEastWest()
        {
            return this.ewPosition;
        }
        public int getWaypointEW()
        {
            return this.wayPointEW;
        }
        public int getWaypointNS()
        {
            return this.wayPointNS;
        }
    }
    

    public static void main(String[] args) {
        
        List<String> commands = new ArrayList<String>();
        try
        {
            String filePath = AdventUtil.getPathTo("day12_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                commands.add(line);
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        MyGps gps = new MyGps();
        for (String line : commands)
        {
            char command = line.charAt(0);
            int value = Integer.parseInt(line.substring(1));
            gps.doCommand(command, value);
            //System.out.println(command + ":" + value + " ->(" + gps.getEastWest() + "," + gps.getNorthSouth() +") [" + gps.getWaypointEW() +"," + gps.getWaypointNS() +"]");
        }
                
        System.out.println(Math.abs(gps.getNorthSouth()) + Math.abs(gps.getEastWest()));
    }

}
