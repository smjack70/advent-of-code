import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class day23b {
    
    private static class CircleNode
    {
        int value = 0;
        private CircleNode next = null;
        private CircleNode previous = null;
        
        public CircleNode(int value)
        {
            this.value = value;
            this.next = this;
            this.previous = next;
        }
        
        public void addAfter(CircleNode item)
        {
            item.next = this.next;
            item.previous = this;
            item.next.previous = item;
            this.next = item;
        }
        
        public CircleNode removeNextNode()
        {
            CircleNode remove = this.next;
            this.next = remove.next;
            remove.next.previous = this;
            remove.next = null;
            remove.previous = null;
            return remove;
        }
        
        public CircleNode getNext()
        {
            return next;
        }
        
        public CircleNode getPrevious()
        {
            return previous;
        }
        
        public int getValue()
        {
            return value;
        }
        
    }
    
    private static void insertNodes(CircleNode current, CircleNode c1, CircleNode c2, CircleNode c3, Map<Integer, CircleNode> index)
    {
        CircleNode pointer = null;
        int val = current.getValue() -1;
        while (true)
        {
            if (0 >= val)
            {
                val = 1000000;
            }
            
            pointer = index.get(val);
            if (pointer == c1 || pointer == c2 || pointer == c3)
            {
                val = val -1;
                continue;
            }
            pointer.addAfter(c3);
            pointer.addAfter(c2);
            pointer.addAfter(c1);
            break;
            /*
            pointer = pointer.getNext();
            if (pointer == current)
            {
                val = val -1;
                
            }
            else if (pointer.getValue() == val)
            {
                pointer.addAfter(c3);
                pointer.addAfter(c2);
                pointer.addAfter(c1);
                break;
            }
            */
        }
    }
    
    private static void printCircle(CircleNode start)
    {
        CircleNode pointer = start;
        do
        {
            System.out.print(pointer.getValue());
            pointer = pointer.getNext();
        }
        while (pointer != start);
        System.out.println("");
            
    }

    public static void main(String[] args) {

        CircleNode current = null;
        Map<Integer, CircleNode> circleIndex = new HashMap<Integer, CircleNode>();
        try
        {
            String filePath = AdventUtil.getPathTo("day23_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            line = reader.readLine(); // Read off the player line
            for (char x : line.toCharArray())
            {
                CircleNode nextItem = new CircleNode(Character.getNumericValue(x));
                if (null == current)
                {
                    current = nextItem;
                }
                else
                {
                    current.previous.addAfter(nextItem);
                }
                circleIndex.put(nextItem.getValue(), nextItem);
            }
            
            for (int i = 10; i <= 1000000; i++)
            {
                CircleNode next = new CircleNode(i);
                current.previous.addAfter(next);
                circleIndex.put(next.getValue(),  next);
            }
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        for (long i = 0; i < 10000000; i++)
        {
            CircleNode c1 = current.removeNextNode();
            CircleNode c2 = current.removeNextNode();
            CircleNode c3 = current.removeNextNode();
            insertNodes(current, c1, c2, c3, circleIndex);
            current = current.getNext();
        }
        

        current = circleIndex.get(1);

        long val = 1;
        val *= current.getNext().getValue();
        val *= current.getNext().getNext().getValue();
        System.out.println(val);
        
        
        
    } // end main

}
