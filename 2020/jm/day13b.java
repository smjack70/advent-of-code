import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day13b {
    
    static String OUT_OF_SERVICE = "x";
    
    static int INDEX_START = 0;
    static int INDEX_INCREM = 1;
    
    private static boolean scheduleMatch(long  timestamp, List<Long> busIds)
    {
        for (int i = 0; i < busIds.size(); i++)
        {
            if (busIds.get(i) == null)
            {
                continue;
            }
            long value = busIds.get(i);
            if ( ( (timestamp + i) % value) != 0)
            {
                return false;
            }
        }
        return true;
    }
    
    
    // This works. It probably shouldn't. What I should do is find the prime 
    // numbers within the sequence and use those to figure the start position 
    // and increment. But fuck it: this worked.
    private static long[] getIncrement(List<Long> busIds)
    {
        long[] start = new long[2];
        start[INDEX_START] = 0;
        start[INDEX_INCREM] = 0;
        
        long first = busIds.get(0); // This is sorta cheating. Should probably generalize this to not assume the 1st value is at index 0;
        for (int i = 1; i < busIds.size(); i++)
        {
            Long value = busIds.get(i);
            if (null == value)
            {
                continue;
            }
            long val = (first * value.longValue()) - i;
            if (val > start[INDEX_INCREM])
            {
                start[INDEX_START] = val;
                start[INDEX_INCREM] = first * value.longValue();
                
            }
        }
        return start;
    }
    
    
    
    public static void main(String[] args) {
        
        List<Long> busIds = new ArrayList<Long>();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day13_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            reader.readLine(); // We don't care about the first line
            String[] schedule = reader.readLine().split(",");
            for (String busId : schedule)
            {
                if ( busId.equals(OUT_OF_SERVICE) )
                {
                    busIds.add(null);
                }
                else
                {
                    busIds.add(Long.parseLong(busId));
                }
            }

            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        long[] increm = getIncrement(busIds);
        long timestamp = increm[INDEX_START];
        
        while (timestamp > 0 && false == scheduleMatch(timestamp, busIds) )
        {
            timestamp += increm[INDEX_INCREM];
        }
        System.out.println(timestamp);

    }

}
