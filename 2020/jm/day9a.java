import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day9a {

    static boolean validateNumber(List<Long> sequence, Long number, int startIndex, int lookSpace)
    {
        for (int i = startIndex; i < startIndex + lookSpace - 1; i++)
        {
            long first = sequence.get(i);
            for (int j = startIndex + 1; j < startIndex + lookSpace; j++)
            {
                long second = sequence.get(j);
                if ( (first + second) == number)
                {
                    return true;
                }
                    
            }
        }
        
        return false;
    }

    public static void main(String[] args) {
        
        List<Long> stack = new ArrayList<Long>();
        try
        {
            String filePath = AdventUtil.getPathTo("day9_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                stack.add(Long.parseLong(line));
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int lookSpace = 25;
        int startIndex = 0; // indexing starts at 0;
        while ( (startIndex + lookSpace) < stack.size())
        {
            long currentNum = stack.get(startIndex + lookSpace);
            if (false == validateNumber(stack, currentNum, startIndex, lookSpace))
            {
                System.out.println(currentNum);
                break;
            }
            startIndex += 1;
        }
                
    }

}
