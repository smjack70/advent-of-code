
public class HairValidator implements ValidateData {

	@Override
	public boolean validate(String data){
		if (data.length() != 7)
		{
			return false;
		}
		return data.matches("^#[0-9a-f]{6}$");


	}

}
