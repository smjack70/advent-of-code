import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class day6a {

    public static void main(String[] args) {
        
        List<Set<Character>> customsForms = new ArrayList<Set<Character>>();
        try
        {
            String filePath = AdventUtil.getPathTo("day6_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            
            Set<Character> current = null;
            while ((line = reader.readLine()) != null)
            {
                if (line.isEmpty() == true)
                {
                    if (current != null)
                    {
                        customsForms.add(current);
                        current = null;
                    }
                    continue;
                }
                if (current == null)
                {
                    current = new HashSet<Character>();
                }
                for (int i = 0; i < line.length(); i++)
                {
                    current.add(line.charAt(i));
                }
            }
            
            if (current != null)
            {
                customsForms.add(current);
                current = null;
            }
            
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int total = 0;
        for (int i = 0; i < customsForms.size(); i++)
        {
            total = total + customsForms.get(i).size();
        }
        System.out.println(total);
                
    }

}
