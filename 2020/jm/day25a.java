import java.io.BufferedReader;
import java.io.FileReader;

public class day25a {
    
    private static final long SUBJECT = 7;
    private static final long DIVISOR = 20201227;
    private static long findLoopSize(long publicKey)
    {
        long value = 1;
        int loopSize = 0;
        while (value != publicKey)
        {
            value = value * SUBJECT;
            value = value % DIVISOR;
            loopSize++;
        }
        return loopSize;
    }
    
    private static long transform(long subject, long loopSize)
    {
        long value = 1;
        for (int i = 0; i < loopSize; i++)
        {
            value = value * subject;
            value = value % DIVISOR;
        }
        return value;
    }
    
    public static void main(String[] args) {

        long pk1 = 0;
        long pk2 = 0;
        try
        {
            String filePath = AdventUtil.getPathTo("day25_input.txt");
            //String filePath = AdventUtil.getPathTo("test2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            
            pk1 = Long.parseLong(reader.readLine());
            pk2 = Long.parseLong(reader.readLine());            
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        long pk1Loop = findLoopSize(pk1);
        long pk2Loop = findLoopSize(pk2);
        
        long pk1Encryption = transform(pk2, pk1Loop);
        long pk2Encryption = transform(pk1, pk2Loop);
        
        System.out.println("Key 1 loop size: " + pk1Loop);
        System.out.println("Key 2 loop size: " + pk2Loop);
        if (pk1Encryption == pk2Encryption)
        {
            System.out.println(pk1Encryption);   
        }
        else
        {
            System.out.println(pk1Encryption + " != " + pk2Encryption ); 
        }
        
    } // end main

}
