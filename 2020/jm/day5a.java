import java.io.BufferedReader;
import java.io.FileReader;

public class day5a {
    
    static char F = 'F'; // Binary 0
    static char B = 'B'; // Binary 1
    
    static char L = 'L'; // Binary 0
    static char R = 'R'; // Binary 1
    
    private static int getSeatNumber(String seatCode)
    {
        int row = 0;
        int column = 0;
        
        for (int i = 0; i < 7; i++)
        {
            row = row << 1;
            if (seatCode.charAt(i) == B) {
                row = row + 1;
            }
        }
        for (int i = 7; i < 10; i++)
        {
            column = column << 1;
            if (seatCode.charAt(i) == R)
            {
                column = column + 1;
            }
        }
        
        return (row * 8) + column;
    }

    
    public static void main(String[] args) {
        
        
        
        try
        {
            String filePath = AdventUtil.getPathTo("day5_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            
            int highestSeat = 0;
            while ((line = reader.readLine()) != null)
            {
                int seatNumber = getSeatNumber(line);
                if(seatNumber > highestSeat)
                {
                    highestSeat = seatNumber;
                }
                
            }
            
            System.out.println(highestSeat);
            
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        
                
    }

}
