import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class day7b {
    
    private static String SHINY_GOLD = "shiny gold";
    private static boolean findShinyGold(String start, Map<String, Map<String, Integer>> bagMap)
    {
        if (bagMap.containsKey(start) == false)
        {
            return false;
        }
        Map<String, Integer> subBags = bagMap.get(start);
        List<String> subKeys = new ArrayList<String>(subBags.keySet());
        if (subKeys.contains(SHINY_GOLD)) 
        {
            return true;
        }
        for (int i = 0; i < subKeys.size(); i++)
        {
            if (findShinyGold(subKeys.get(i), bagMap) == true)
            {
                return true;
            }
        }
        return false;
    }
    
    private static int getSubBagCount(String start, Map<String, Map<String, Integer>> bagMap)
    {
        if (bagMap.containsKey(start) == false)
        {
            return 1;
        }
        int count = 1;
        Map<String, Integer> subBags = bagMap.get(start);
        List<String> subKeys = new ArrayList<String>(subBags.keySet());
        for (int i = 0; i < subKeys.size(); i++)
        {
            String key = subKeys.get(i);
            int multiplier = subBags.get(key);
            count = count + (getSubBagCount(key, bagMap) * multiplier);
        }
        return count;
        
    }

    public static void main(String[] args) {
        
        String BAG_REGEX = " bag[s]?[,|.]";
        String NO_OTHER_BAGS = "no other bags.";
        String BAGS_CONTAINS = " bags contain ";
        Map<String, Map<String, Integer>> bagMap = new HashMap<String, Map<String,Integer>>();
        
        try
        {
            String filePath = AdventUtil.getPathTo("day7_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            
             
            while ((line = reader.readLine()) != null)
            {
                String[] lineElements = line.split(BAGS_CONTAINS);
                if (lineElements.length != 2 || lineElements[1].equals(NO_OTHER_BAGS))
                {
                    continue;
                }
                
                String key = lineElements[0];
                String[] subBags = lineElements[1].split(BAG_REGEX);
                HashMap<String, Integer> bagNums = new HashMap<String, Integer>();
                for (int i = 0; i < subBags.length; i++)
                {
                    String[] y = subBags[i].trim().split(" ", 2);
                    for (int x = 0; x < y.length; x++)
                    {
                        bagNums.put(y[1].trim(), Integer.parseInt(y[0]));
                    }
                }
                bagMap.put(key, bagNums);
                
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int count = getSubBagCount(SHINY_GOLD, bagMap) - 1; // -1 because the shiny gold bag doesn't count
        
        System.out.println(count);
                
    }

}
