import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day9b {

    static boolean validateNumber(List<Long> sequence, Long number, int startIndex, int lookSpace)
    {
        for (int i = startIndex; i < startIndex + lookSpace - 1; i++)
        {
            long first = sequence.get(i);
            for (int j = startIndex + 1; j < startIndex + lookSpace; j++)
            {
                long second = sequence.get(j);
                if ( (first + second) == number)
                {
                    return true;
                }
                    
            }
        }
        
        return false;
    }
    
    static int HIGH_INDEX = 0;
    static int LOW_INDEX = 1;
    static long[] findHighLow(List<Long> sequence, Long targetNum)
    {
        int index = 0;
        long highLow[] = new long[2];
        
        for (index = 0; index < sequence.size(); index ++)
        {
            Long total = 0L;
            int countIndex = index;
            highLow[HIGH_INDEX] = 0;
            highLow[LOW_INDEX] = Long.MAX_VALUE;
            while (total < targetNum && (countIndex < sequence.size()) )
            {
                long next = sequence.get(countIndex);
                total = total + next;
                if (next > highLow[HIGH_INDEX])
                {
                    highLow[HIGH_INDEX] = next;
                }
                if (next < highLow[LOW_INDEX])
                {
                    highLow[LOW_INDEX] = next;
                }
                countIndex += 1;
            }
            if ( total.equals(targetNum) ) // Because fucking Java.
            {
                return highLow;
            }
                    
        }
        return null;
    }

    public static void main(String[] args) {
        
        List<Long> stack = new ArrayList<Long>();
        try
        {
            String filePath = AdventUtil.getPathTo("day9_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                stack.add(Long.parseLong(line));
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int lookSpace = 25;
        int startIndex = 0; // indexing starts at 0;
        long currentNum = 0;
        while ( (startIndex + lookSpace) < stack.size())
        {
            currentNum = stack.get(startIndex + lookSpace);
            if (false == validateNumber(stack, currentNum, startIndex, lookSpace))
            {
                System.out.println(currentNum);
                break;
            }
            startIndex += 1;
        }
        
        long highLow[] = findHighLow(stack,  currentNum);
        if (highLow != null)
        {
            System.out.println(highLow[HIGH_INDEX] + highLow[LOW_INDEX]);
        }
                
    }

}
