import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day2a {

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        try
        {
            String filePath = AdventUtil.getPathTo("day2_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null)
            {
                list.add(line);
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        int counter = 0;
        for (int i = 0; i < list.size(); i++)
        {
            String[] pwItems = list.get(i).split(" ");
            
            String[] minMax = pwItems[0].split("-");
            int min = Integer.parseInt(minMax[0]);
            int max = Integer.parseInt(minMax[1]);
            
            
            String[] pwChar = pwItems[1].split(":");
            char pwC =pwChar[0].charAt(0);

            
            String pw = pwItems[2];
            
            int count = 0;
            for (int j = 0; j < pw.length(); j++)
            {
                if (pw.charAt(j) == pwC)
                {
                    count = count + 1;
                }
                
            }
            
            if (count >= min && count <= max)
            {
                counter = counter + 1;
            }
            
        }
        System.out.println(counter);
    }

}
