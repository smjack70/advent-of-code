import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class day4a {

    public static void main(String[] args) {
        
        List<Map<String, String>> passportList = new ArrayList<Map<String, String>>();
        String[] requiredFields= {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};
        
        try
        {
            String filePath = AdventUtil.getPathTo("day4_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            Map<String, String> passport = null;
            while ((line = reader.readLine()) != null)
            {
                if (line.isEmpty())
                {
                    if (passport != null) {
                        passportList.add(passport);
                        passport = null;
                    }
                    continue;
                }
                
                if (passport == null)
                {
                    passport = new HashMap<String, String>();    
                }

                String[] fields = line.split(" ");
                for (int i = 0; i < fields.length; i++)
                {
                    String[] keyval = fields[i].trim().split(":");
                    passport.put(keyval[0], keyval[1]);
                }
            }
            
            if (passport != null) {
                passportList.add(passport);
                passport = null;
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int valid = 0;
        for (int i = 0; i < passportList.size(); i++)
        {
            boolean hasFields = true;
            Map<String, String> passport = passportList.get(i);
            for (int j = 0; j < requiredFields.length; j++)
            {
                if (passport.containsKey(requiredFields[j]) == false)
                {
                    hasFields = false;
                    break;
                }
            }
            if (hasFields)
            {
                valid = valid + 1;
            }
        }
        System.out.println(valid);
        
                
    }

}
