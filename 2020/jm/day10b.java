import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class day10b {
    
    private static class JoltNode
    {
        private long value = 0;
        private long  weight = 0;
        public JoltNode(long value, long  weight)
        {
            this.value = value;
            this.weight = weight;
        }
        
        public long getValue()
        {
            return this.value;
        }
        
        public long getWeight()
        {
            return this.weight;
        }
        
    }

    private static List<JoltNode> getNodeWeights(List<Long> nodes)
    {
        List<JoltNode> nodeMaps = new ArrayList<JoltNode>();
        
        for (int i = 0; i < nodes.size(); i++)
        {
            long value = nodes.get(i);
            long weight = 0;
            for (int j = 1; j <=3; j++)
            {
                int nextIndex = i + j;
                if (nextIndex >= nodes.size())
                {
                    break;
                }
                long peekValue = nodes.get(nextIndex);
                if (peekValue - value > 3)
                {
                    break;
                }
                weight++;
            }
            // This is for the case where we're the last node in the list. Technically, it still goes somewhere
            if (weight == 0)
            {
                weight = 1;
            }
            nodeMaps.add(new JoltNode(value, weight));
        }
        
        return nodeMaps;
    }
    
    // From a given start point, we're looking for exit "choke points": places
    // where there are no further decisions to be made (weight == 1). These are 
    // going to be the leaf nodes of each decision tree. For each leaf exiting 
    // a supernode, one-up the count. 
    private static long calcSuperNodeValue(List<JoltNode> nodes, int start)
    {
        JoltNode node = nodes.get(start);
        long weight = node.getWeight();
        if (weight == 1)
        {
            return 1;
        }

        long value = 0;
        for (int i = 1; i <= weight  && (start + i) < nodes.size(); i++)
        {
            int subIndex = start + i;
            value += calcSuperNodeValue(nodes, subIndex);
        }

        return value;
    }
    
    
    // From a given start position, find the next node that has a weight of 1.
    // That will be where the current supernode ends.
    private static int getFindSuperNodeEnd(List<JoltNode> nodes, int start)
    {
        int newIndex = start;    
        JoltNode node = nodes.get(newIndex);

        while (node.getWeight() > 1)
        {
            newIndex++;
            node = nodes.get(newIndex);
        }
        
        return newIndex;
    }
    
    // The idea here is that there are supernodes within the list: sequences 
    // with multiple decision points. Each node has a "weight"; the number
    // of other nodes it can jump to. We find the "value" of the super nodes
    // and multiply them together: that should be the total number of distinct
    // paths.
    private static long countSuperNodes(List<JoltNode> nodeList)
    {
        long superCount = 1;
        
        int index = 0;
        while (index < nodeList.size())
        {
            JoltNode node = nodeList.get(index);
            if (node.getWeight() > 1)
            {
                superCount *= calcSuperNodeValue(nodeList, index);
                index = getFindSuperNodeEnd(nodeList, index);
            }
            else
            {
                index = index + 1;
            }
        }
        
        return superCount;
    }
    
    public static void main(String[] args) {
        
        List<Long> adapters = new ArrayList<Long>();
        adapters.add(0L); // Add the "plug"
        try
        {
            String filePath = AdventUtil.getPathTo("day10_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = reader.readLine()) != null)
            {
                adapters.add(Long.parseLong(line));
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        Collections.sort(adapters);
        List<JoltNode> nodeWeights = getNodeWeights(adapters);
        long count = countSuperNodes(nodeWeights);

        System.out.println(count);
       
    }

}
