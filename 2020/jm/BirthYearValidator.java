
public class BirthYearValidator implements ValidateData {

	@Override
	public boolean validate(String data) {
		if (data.length() != 4)
		{
			return false;
		}
		try
		{
			int year = Integer.parseInt(data);
			if ((year < 1920) || (year > 2002))
			{
				return false;
			}
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}

}
