import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class day19a {
    
    public static enum RuleType
    {
        VALUE,
        REFERENCE,
        UNKNOWN
    };
    
    private static class Rule
    {
        private RuleType type = RuleType.UNKNOWN;
        private int key;
        
        private List<List<Integer>> references = new ArrayList<List<Integer>>();
        private char value = ' ';
        
        public Rule(String rule)
        {
            String[] parts = rule.split(":");
            this.key = Integer.parseInt(parts[0]);
            
            String part1 = parts[1].trim();
            
            if (part1.charAt(0) == '"')
            {
                this.value = part1.charAt(1);
                this.type = RuleType.VALUE;
            }
            else
            {
                String[] keySets = part1.split("[|]");
                for (String keySet : keySets)
                {
                    List<Integer> keyList = new ArrayList<Integer>();
                    String[] keys = keySet.trim().split(" ");
                    for (String key : keys)
                    {
                        keyList.add(Integer.parseInt(key));
                    }
                    references.add(keyList);
                }
                this.type = RuleType.REFERENCE;
            }

        }
        
        public RuleType getRuleType()
        {
            return this.type;
        }
        
        public int getKey()
        {
            return this.key;
        }
        
        public List<List<Integer>> getReferences()
        {
            return this.references;
        }
        
        public char getValue()
        {
            return this.value;
        }
    }

    private static class RuleEngine
    {
        private Map<Integer, Rule> rules = null;
        private Map<Integer, List<String>> matches = null;
        public RuleEngine()
        {
            this.rules = new HashMap<Integer, Rule>();
            this.matches = new HashMap<Integer, List<String>>();
        }
        
        public void addRule(Rule rule)
        {
            rules.put(rule.getKey(), rule);
        }
        
        private List<String> assemblePatterns(List<Integer> keys, String pattern)
        {
            List<String> patterns = new ArrayList<String>();
            patterns.add(pattern);
            for (int key : keys)
            {
                Rule rule = rules.get(key);
                switch (rule.getRuleType()) 
                {
                case VALUE:
                    char value = rule.getValue();
                    for (int i = 0; i < patterns.size(); i++)
                    {
                        patterns.set(i, patterns.get(i) + value);
                    }
                    break;
                case REFERENCE:
                    List<String> newPatterns = new ArrayList<String>();
                    List<List<Integer>> keySets = rule.getReferences();
                    for (List<Integer> keySet : keySets)
                    {
                        for (String p : patterns)
                        {
                            newPatterns.addAll(assemblePatterns(keySet, p));
                        }
                    }
                    patterns = newPatterns;
                    break;
                default:
                    break;
                }
                
            }
            return patterns;
        }
        
        private List<String> assemblePatterns(int key, String pattern)
        {
            List<Integer> keys = new ArrayList<Integer>();
            keys.add(key);
            return assemblePatterns(keys, pattern);
        }
        
        private void buildRulePatterns(int key)
        {
            List<String> patterns = assemblePatterns(key, ""); 
            this.matches.put(key, patterns);
        }
        
        
        public boolean matchRule(int key, String value)
        {
            if (!this.matches.containsKey(key))
            {
                buildRulePatterns(key);
            }
            List<String> patterns = this.matches.get(key);
            for (String pattern : patterns)
            {
                if (pattern.equals(value))
                {
                    return true;
                }
            }
            
            return false;
        }
        
    }

    public static void main(String[] args) {

        RuleEngine rules = new RuleEngine();
        List<String> patterns = new ArrayList<String>();
        try
        {
            String filePath = AdventUtil.getPathTo("day19_input.txt");
            //String filePath = AdventUtil.getPathTo("test.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            // Read rules
            while (!(line = reader.readLine()).isEmpty())
            {
                Rule rule = new Rule(line);
                rules.addRule(rule);
            }
            while ((line = reader.readLine()) != null)
            {
                patterns.add(line);
            }
            
            reader.close();
   
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        int matchCount = 0;
        for (String pattern : patterns)
        {

            if ( rules.matchRule(0, pattern) == true)
            {
                matchCount++;
            }
        }
        System.out.println(matchCount);
        
    } // end main

}
