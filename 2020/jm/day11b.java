import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class day11b {
    
    private static final char FLOOR    = '.';
    private static final char OPEN     = 'L';
    private static final char OCCUPIED = '#';
    
    private static final int GO_NORTH = -1;
    private static final int GO_SOUTH = 1;
    private static final int GO_WEST = -1;
    private static final int GO_EAST = 1;
    private static final int GO_NOWHERE = 0;
    
    private static class HashedGrid
    {
        private Character[][] grid = null;
        HashedGrid(int rows, int columns)
        {
            this.grid = new Character[rows][columns];
        }
        
        HashedGrid(HashedGrid copy)
        {
            int rows = copy.getNumRows();
            int cols = copy.getNumColumns();
            this.grid = new Character[rows][cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    this.grid[i][j] = copy.getValue(i, j);
                }
            }
        }
        
        public void setValue(Character value, int row, int col)
        {
            this.grid[row][col] = value;
        }
        
        public char getValue(int row, int col)
        {
            return this.grid[row][col];
        }
        
        public int hashCode()
        {
            StringBuffer buffer = new StringBuffer();
            for (int row = 0; row < grid.length; row++)
            {
                for (int col = 0; col < grid[row].length; col++)
                {
                    buffer.append(grid[row][col]);
                }
            }
            return buffer.toString().hashCode();
        }
        
        public int getNumRows()
        {
            return grid.length;
        }
        
        public int getNumColumns()
        {
            return grid[0].length; 
        }
        
        public void printGrid()
        {
            for (int i = 0; i < this.getNumRows(); i++)
            {
                for (int j = 0; j < this.getNumColumns(); j++)
                {
                    System.out.print(this.getValue(i, j));
                }
                System.out.println("");
            }
        }
        
    }
    
    private static boolean isFirstSeatSeenOccupied(HashedGrid grid, int startX,int xIncr, int startY, int yIncr)
    {
        int x = startX;
        int y = startY;
        while (true)
        {
            x = x + xIncr;
            y = y + yIncr;
            
            if ( x < 0 || y < 0 || x >= grid.getNumRows() || y >= grid.getNumColumns())
            {
                // Off the grid. 
                break;
            }
            switch (grid.getValue(x, y))
            {
            case OCCUPIED:
                return true;
            case OPEN:
                return false;
            default:
                break;
            }
        }
        return false;
    }
    
    private static void updateSeats(HashedGrid seats)
    {
        HashedGrid copy = new HashedGrid(seats);
        int rows = copy.getNumRows();
        int cols = copy.getNumColumns();
        
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                char val = copy.getValue(i, j);
                if (val == FLOOR)
                {
                    continue;
                }
                int occupied = 0;
                //NorthWest
                if ( isFirstSeatSeenOccupied(copy, i, GO_NORTH, j, GO_WEST) )
                {
                    occupied++;
                }
                // North
                if (isFirstSeatSeenOccupied(copy, i, GO_NORTH, j, GO_NOWHERE))
                {
                    occupied++;
                }
                // NorthEast
                if (isFirstSeatSeenOccupied(copy, i, GO_NORTH, j, GO_EAST))
                {
                    occupied++;
                }
                // West
                if (isFirstSeatSeenOccupied(copy, i, GO_NOWHERE, j, GO_WEST))
                {
                    occupied++;
                }
                // East
                if (isFirstSeatSeenOccupied(copy, i, GO_NOWHERE, j, GO_EAST))
                {
                    occupied++;
                }
                // SouthWest
                if (isFirstSeatSeenOccupied(copy, i, GO_SOUTH, j, GO_WEST))
                {
                    occupied++;
                }
                // South
                if (isFirstSeatSeenOccupied(copy, i, GO_SOUTH, j, GO_NOWHERE))
                {
                    occupied++;
                }
                // SouthEast
                if (isFirstSeatSeenOccupied(copy, i, GO_SOUTH, j, GO_EAST))
                {
                    occupied++;
                }
                
                switch(val)
                {
                case OCCUPIED:
                    if (occupied >= 5)
                    {
                        seats.setValue(OPEN, i, j);
                    }
                    break;
                case OPEN:
                    if (occupied == 0)
                    {
                        seats.setValue(OCCUPIED, i, j);
                    }
                    break;
                default:
                    break;
                }
                
            }
        }
        
    }

    public static void main(String[] args) {
        
        HashedGrid seatGrid = null;
        
        try
        {
            String filePath = AdventUtil.getPathTo("day11_input.txt");
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;

            List<List<Character>> seats = new ArrayList<List<Character>>();
            while ((line = reader.readLine()) != null)
            {
                List<Character>floorLine = new ArrayList<Character>();
                for (int i = 0; i < line.length(); i++)
                {
                    floorLine.add(line.charAt(i));
                }
                
                seats.add(floorLine);
            }
            reader.close();
            
            int rows = seats.size();
            for (int i = 0; i < rows; i++)
            {
                List<Character> row = seats.get(i);
                int cols = row.size();
                if (seatGrid == null)
                {
                    seatGrid = new HashedGrid(rows, cols);
                }
                for (int j = 0; j < cols; j++)
                {
                    seatGrid.setValue(row.get(j), i, j);
                }
            }
                
        }
        catch (Exception e)
        {
            System.out.println(e);
            System.exit(0);
        }
        
        // Do Exercise
        
        int hashValue = 0;
        do
        {
            hashValue = seatGrid.hashCode();
            updateSeats(seatGrid);
        } while (hashValue != seatGrid.hashCode());
        
        int occupiedSeats = 0;
        for (int i = 0; i < seatGrid.getNumRows(); i++)
        {
            for (int j = 0; j < seatGrid.getNumColumns(); j++)
            {
                if (seatGrid.getValue(i, j) == OCCUPIED)
                {
                    occupiedSeats++;
                }
            }
        }
        System.out.println(occupiedSeats);
                
    }

}
