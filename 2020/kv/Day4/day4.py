with open("day4_input.txt") as file:
    data = file.readlines()

def part1(data):
    valid_count, invalid_count = 0,0
    passport = {}
    req_fields = ["byr","iyr","eyr","hgt","hcl","ecl","pid"]
    for line in data + ["\n"]:
        if line == "\n":
            if all(field in passport for field in req_fields):    
                valid_count += 1
            else:
                invalid_count += 1
            passport = {}
        else:
            fields = line.split(" ")
            for field in fields:
                k,v = field.split(":")
                passport[k] = v.strip("\n")
    return valid_count

def is_invalid_byr(val):
    result = False
    if (len(val) != 4) or (int(val) < 1920) or (int(val) > 2002):
        result = True
    return result

def is_invalid_iyr(val):
    result = False
    if (len(val) != 4) or (int(val) < 2010) or (int(val) > 2020):
        result = True
    return result

def is_invalid_eyr(val):
    result = False
    if (len(val) != 4) or (int(val) < 2020) or (int(val) > 2030):
        result = True
    return result

def is_invalid_hgt(val):
    result = False
    if val.endswith("cm") or val.endswith("in"):
        unit = val[-2:]
        num = int(val[:-2])
        if unit == "cm":
            if (num < 150) or (num > 193):
                result = True
        elif unit == "in":
            if (num < 59) or (num > 76):
                result = True
    else:
        result = True
    return result

def is_invalid_hcl(val):
    result = False
    if val.startswith("#") and (len(val) == 7):
        color = val[1:]
        for char in color:
            if char not in 'abcdef1234567890':
                result = True
                break
    else:
        result = True
    return result

def is_invalid_ecl(val):
    result = False
    if len(val) != 3:
        result = True
    else:
        if val not in ['amb','blu','brn','gry','grn','hzl','oth']:
            result = True
    return result

def is_invalid_pid(val):
    result = False
    if len(val) == 9:
        try:
            int(val)
        except ValueError:
            result = True
    else:
        result = True
    return result

def validate_fields(passport):
    valid = True
    for field in passport:
        if field=="byr":
            invalid = is_invalid_byr(passport[field])
        elif field=="iyr":
            invalid = is_invalid_iyr(passport[field])
        elif field=="eyr":
            invalid = is_invalid_eyr(passport[field])
        elif field=="hgt":
            invalid = is_invalid_hgt(passport[field])
        elif field=="hcl":
            invalid = is_invalid_hcl(passport[field])
        elif field=="ecl":
            invalid = is_invalid_ecl(passport[field])
        elif field=="pid":
            invalid = is_invalid_pid(passport[field])
        elif field=="cid":
            invalid = False
        else:
            invalid = True
        if invalid:
            break
    valid = not invalid
    return valid

def part2(data):
    valid_count, invalid_count = 0,0
    passport = {}
    req_fields = ["byr","iyr","eyr","hgt","hcl","ecl","pid"]
    for line in data:
        if line == "\n":
            if all(field in passport for field in req_fields):
                valid_fields = validate_fields(passport)
                if valid_fields:
                    valid_count += 1
                else:
                    invalid_count += 1
            else:
                invalid_count += 1
            passport = {}
        else:
            fields = line.split(" ")
            for field in fields:
                k,v = field.split(":")
                passport[k] = v.strip("\n")
    return valid_count

print(part1(data)) #Output: 256
print(part2(data)) #Output: 198
