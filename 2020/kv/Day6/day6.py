with open("day6_input.txt") as file:
    data = file.readlines()

def part1(data):
    questions = set()
    group_counts = []
    for line in data + ["\n"]:
        if line == "\n":
            group_counts.append(len(questions))
            questions.clear()
        else:
            person = line.rstrip()
            for char in person:
                questions.add(char)
    return sum(group_counts)

def process_group(group):
    questions = dict()
    if len(group) == 1:
        count = len(group[0])
    else:
        for person in group:
            for char in person:
                if char in questions:
                    questions[char] += 1
                else:
                    questions[char] = 1
        common = [x for x in questions if questions[x] == len(group)]
        count = len(common)
    return count

def part2(data):
    group_counts = []
    group = []
    for line in data + ["\n"]:
        if line == "\n":
            group_counts.append(process_group(group))
            group.clear()
        else:
            person = line.rstrip()
            group.append(person)
    return sum(group_counts)

print(part1(data)) #Output: 6273
print(part2(data)) #Output: 3254