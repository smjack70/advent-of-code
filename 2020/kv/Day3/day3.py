with open("day3_input.txt") as file:
    data = file.readlines()

def part1(data,x_incr,y_incr):
    count,x = 0,0
    line_length = len(data[0].rstrip())
    for y in range(0,len(data),y_incr):
        new_line = data[y].rstrip()
        if new_line[x%line_length] == "#":
            count += 1
        x+=x_incr
    return count

incr_list = [(1,1),(3,1),(5,1),(7,1),(1,2)]
def part2(incr_list):
    result = 1
    for i in incr_list:
        output = part1(data,i[0],i[1])
        result*=output
    return result

print(part1(data,1,1))  #Output: 187
print(part2(incr_list)) #Output: 4723283400
