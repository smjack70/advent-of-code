with open("day5_input.txt") as file:
    data = file.readlines()

def determine_row(row_code, total_rows):
    lower = 0
    upper = total_rows - 1
    for char in row_code:
        if char == "F":
            upper = int((lower+upper)/2)
            result = upper
        else:
            lower = int((lower+upper)/2) + 1
            result = lower
    return result

def determine_col(col_code, total_cols):
    lower = 0
    upper = total_cols - 1
    for char in col_code:
        if char == "L":
            upper = int((lower+upper)/2)
            result = upper
        else:
            lower = int((lower+upper)/2) + 1
            result = lower
    return result

def determine_seat_id(row,col):
    return row * 8 + col

def part1(data):
    total_rows = 128
    total_cols = 8
    seat_ids = []
    for line in data:
        boarding_pass = line.rstrip()
        row_code = boarding_pass[:7]
        col_code = boarding_pass[7:]
        row = determine_row(row_code, total_rows)
        col = determine_col(col_code, total_cols)
        seat_id = determine_seat_id(row,col)
        seat_ids.append(seat_id)
    return max(seat_ids)

def part2(data):
    total_rows = 128
    total_cols = 8
    seat_ids = []
    for line in data:
        boarding_pass = line.rstrip()
        row_code = boarding_pass[:7]
        col_code = boarding_pass[7:]
        row = determine_row(row_code, total_rows)
        col = determine_col(col_code, total_cols)
        seat_id = determine_seat_id(row,col)
        seat_ids.append(seat_id)
    seat_ids.sort()
    missing = [x for x in range(seat_ids[0], seat_ids[-1]+1) if x not in seat_ids]
    return missing[0]

print(part1(data)) #Output: 963
print(part2(data)) #Output: 592