with open("day2_input.txt") as file:
    data = file.readlines()

def part1(data):
    valid_count = 0 
    for line in data:
        [policy, letter, value] = line.split(" ")
        [lower, upper] = policy.split("-")
        letter = letter[0]
        count = value.count(letter)
        if (count >= int(lower)) and (count <= int(upper)):
            valid_count += 1
    return valid_count

def part2(data):
    valid_count = 0
    for line in data:
        [policy, letter, value] = line.split(" ")
        [pos1, pos2] = policy.split("-")
        letter = letter[0]
        if (value[int(pos1)-1] == letter) ^ (value[int(pos2)-1] == letter):
            valid_count += 1
    return valid_count

print(part1(data)) #Output: 445
print(part2(data)) #Output: 491