with open("day1_input.txt") as file:
    data = file.readlines()

#Part 1
def part1(data):
    for val1 in data:
        for val2 in data:
            if int(val1) + int(val2) == 2020:
                return (int(val1) * int(val2))

#Part 2
def part2(data):
    for val1 in data:
        for val2 in data:
            for val3 in data:
                if int(val1) + int(val2) + int(val3) == 2020:
                    return (int(val1) * int(val2) * int(val3))

print(part1(data)) #Output: 713184
print(part2(data)) #Output: 261244452