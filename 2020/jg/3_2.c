#include <stdio.h>
#include <regex.h>

int main()
{
    //Globals
    const char* format = "%s"; 
    const char* fileName = "3_1.input";
    const int columnCount = 31, rowCount = 323;
    char course[columnCount];
    char map[rowCount][columnCount];
    //Check file existence
    FILE *inFile = fopen(fileName, "r");
    if (!inFile){
        printf("%s does not exist!\n", fileName);
        return -1;
    }

    int rowIndex = 0;
    while(fscanf(inFile, format, course) == 1){
       for (int colIndex = 0; colIndex < columnCount; colIndex++){
           map[rowIndex][colIndex] = course[colIndex];
       }
       rowIndex += 1;
    }
    
    
    int slopes[5][2] = {
        {1, 1},
        {3, 1},
        {5, 1},
        {7, 1},
        {1, 2},
    };
    u_int64_t product = 1;
    for (int slopeIndex = 0; slopeIndex < 5; slopeIndex ++){
        int treeCounter = 0;
        int colIndex = 0;
        for (rowIndex = 0; rowIndex < rowCount; rowIndex += slopes[slopeIndex][1]){
            if (map[rowIndex][colIndex % columnCount] == '#'){
                treeCounter += 1;
            }
            colIndex += slopes[slopeIndex][0];
        }
        product *= treeCounter;
        printf("%ld %d\n", product, treeCounter);
    }
    printf("%ld trees!\n", product);
    return 0;
}