#include <stdio.h>
#include <regex.h>

int main()
{
    //Globals
    const char* format = "%d-%d %c: %s";
    const char fileName[] = "2_1.input";
    const int pwordSize = 50;
    char pword[pwordSize];
    int first = 0, second = 0, valid = 0, total = 0;
    char letter;    

    //Check file existence
    FILE *inFile = fopen(fileName, "r");
    if (!inFile){
        printf("%s does not exist!\n", fileName);
        return -1;
    }

    while(fscanf(inFile, format, &first, &second, &letter, pword) == 4){
        total += 1; 
        first -= 1;
        second -= 1;  
        printf("%d %d %c %c %c %s\n", first, second, pword[first], pword[second], letter, pword);             
        if ((pword[first] == letter) ^ (pword[second] == letter)){
            valid += 1;
        }
    }
    printf("Total: %d\nValid: %d\n", total, valid);
    return 0;
}