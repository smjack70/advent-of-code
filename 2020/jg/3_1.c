#include <stdio.h>
#include <regex.h>

int main()
{
    //Globals
    const char* format = "%s"; 
    const char* fileName = "3_1.input";
    const int columnCount = 31, rowCount = 323;
    char course[columnCount];
    char map[rowCount][columnCount];
    //Check file existence
    FILE *inFile = fopen(fileName, "r");
    if (!inFile){
        printf("%s does not exist!\n", fileName);
        return -1;
    }

    int rowIndex = 0;
    while(fscanf(inFile, format, course) == 1){
       for (int colIndex = 0; colIndex < columnCount; colIndex++){
           map[rowIndex][colIndex] = course[colIndex];
       }
       rowIndex += 1;
    }
    
    int treeCounter = 0;
    int colIndex = 0;
    for (rowIndex = 0; rowIndex < rowCount; rowIndex++){
        if (map[rowIndex][colIndex % columnCount] == '#'){
            treeCounter += 1;
        }
        printf("%d %d %c %d\n", rowIndex, colIndex % columnCount, map[rowIndex][colIndex % columnCount], treeCounter);
        colIndex += 3;
    }
    printf("%d trees!\n", treeCounter);
    return 0;
}