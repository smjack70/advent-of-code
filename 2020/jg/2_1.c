#include <stdio.h>
#include <regex.h>

int main()
{
    //Globals
    const char* format = "%d-%d %c: %s";
    const char fileName[] = "2_1.input";
    const int pwordSize = 50;
    char pword[pwordSize];
    int min = 0, max = 0, valid = 0, total = 0;
    char letter;    

    //Check file existence
    FILE *inFile = fopen(fileName, "r");
    if (!inFile){
        printf("%s does not exist!\n", fileName);
        return -1;
    }

    while(fscanf(inFile, format, &min, &max, &letter, pword) == 4){
        int counter = 0;
        total += 1;
        for(int i = 0; i < pwordSize; i++){
            if (pword[i] == letter){
                counter += 1;
            }else if (pword[i] == '\0'){
                break;
            }
        }
        if (counter >= min && counter <= max){
            valid += 1;
        }
    }
    printf("Total: %d\nValid: %d\n", total, valid);
    return 0;
}