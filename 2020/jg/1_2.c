#include <stdio.h>

int main()
{
    //Read input from file
    FILE *in_file = fopen("1_1.input", "r");
    const int input_size = 200;
    //Tried setting numbers to a const int variable but the compiler complained
    int numbers[input_size]; 
    int index = 0;
    int current_int;
    if (!in_file){
        printf("1_1.input does not exist!");
        exit(-1);
    }

    while(fscanf(in_file, "%d", &current_int) == 1){
        numbers[index] = current_int;
        index += 1;
    }

    //Loop over each number and if less than 2020/2 try against all numbers > 2020/2
    for (int i = 0; i< input_size; ++i){
        if (i < 1011){
            for (int j = 0; j < input_size; j++){
                //At least two must be < 1010 ... this is a hack ... do better
                if (j < 1010){
                    for (int k = 0; k < input_size; k++){
                        if(numbers[i] + numbers[j] + numbers[k] == 2020){
                            printf("%d * %d * %d = %d\n", numbers[i], numbers[j], numbers[k], numbers[k] * numbers[i] * numbers[j]);
                            exit(0);
                        }                        
                    }
                }
            }
        }
    }
}
