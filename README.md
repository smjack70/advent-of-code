# Advent of Code

Googles Advent of Code event can be found here:

https://adventofcode.com/

The goal here is to do the Advent coding exercises and share them. This isn't a competition: it's an opportunity to get a little bit of coding practice in, using whatever languages you choose, and then see how others tackled the same problems.